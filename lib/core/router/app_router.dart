import 'package:flutter/material.dart';
import 'package:nwwwls_app/core/screens/settings_screen.dart';
import 'package:nwwwls_app/measurement/presentation/screens/new_data_screen.dart';
import 'package:nwwwls_app/measurement/presentation/screens/update_data_screen.dart';
import 'package:nwwwls_app/measurement/presentation/screens/view_data_form_screen.dart';

import '../../authentication/presentation/screens/forgot_password_screen.dart';
import '../../authentication/presentation/screens/logged_in_screen.dart';
import '../../authentication/presentation/screens/login_screen.dart';
import '../../authentication/presentation/screens/new_password_screen.dart';

class AppRouter {
  Route onGenerateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => const LoginScreen());
      case '/login':
        return MaterialPageRoute(builder: (_) => const LoginScreen());
      case '/logged-in':
        return MaterialPageRoute(builder: (_) => const LoggedInScreen());
      case '/forgot-password':
        return MaterialPageRoute(builder: (_) => const ForgotPasswordScreen());
      case '/new-password':
        return MaterialPageRoute(builder: (_) => const NewPasswordScreen());
      case '/new-data':
        return MaterialPageRoute(builder: (_) => const NewDataScreen());
      case '/update-data':
        return MaterialPageRoute(builder: (_) => const UpdateDataScreen());
      case '/view-data':
        return MaterialPageRoute(builder: (_) => const ViewDataFormScreen());
      case '/settings':
        return MaterialPageRoute(builder: (_) => const SettingsScreen());
      default:
        return MaterialPageRoute(builder: (_) => const LoginScreen());
    }
  }
}
