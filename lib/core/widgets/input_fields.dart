import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

String? validateOptional(String? value) {
  if (value != null && value.isNotEmpty) {
    final numeric = num.tryParse(value);
    if (numeric == null) {
      return 'Please enter a valid number';
    }
  }
  return null;
}

String convertDateFormat(String inputDate) {
  try {
    final parts = inputDate.split('-');
    if (parts.length == 3) {
      final year = parts[0];
      final month = parts[1];
      final day = parts[2];
      return '$month/$day/$year';
    }
  } catch (e) {
    if (kDebugMode) {
      print('Error converting date format: $e');
    }
  }
  return inputDate; // Return the input date if conversion fails
}

DateTime parseDate(String date) {
  final parts = date.split('/');
  if (parts.length != 3) {
    throw FormatException('Invalid date format: $date');
  }
  final month = int.parse(parts[0]);
  final day = int.parse(parts[1]);
  final year = int.parse(parts[2]);
  return DateTime(year, month, day);
}

TimeOfDay? parseTime(String time) {
  time = time.trim(); // Remove leading/trailing whitespace

  if (time.isNotEmpty) {
    // Parse the time if the input is not empty
    List<String> parts = time.split(':');
    if (parts.length == 2) {
      int? hour = int.tryParse(parts[0]);
      int? minute = int.tryParse(parts[1]);

      if (hour != null && minute != null) {
        return TimeOfDay(hour: hour, minute: minute);
      }
    }
  }

  return null; // Return null if the input is empty or invalid
}

class EmailField extends StatelessWidget {
  final TextEditingController? controller;
  const EmailField({
    super.key,
    this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      textInputAction: TextInputAction.next,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Email address',
        prefixIcon: Icon(Icons.email_outlined),
      ),
    );
  }
}

class PasswordField extends StatefulWidget {
  const PasswordField({
    super.key,
    required this.labelText,
  });
  final String labelText;

  @override
  State<PasswordField> createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  bool isHidden = false;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.done,
      decoration: InputDecoration(
          border: const OutlineInputBorder(),
          labelText: widget.labelText,
          prefixIcon: const Icon(Icons.lock_outline),
          suffixIcon: IconButton(
            icon: isHidden
                ? const Icon(Icons.visibility_outlined)
                : const Icon(Icons.visibility_off_outlined),
            onPressed: () {
              setState(() {
                isHidden = !isHidden;
              });
            },
          )),
      obscureText: isHidden,
    );
  }
}

class CommentsField extends StatelessWidget {
  final TextEditingController controller;
  const CommentsField({
    super.key,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 120,
      child: TextFormField(
        controller: controller,
        maxLines: null,
        scrollPadding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        expands: true,
        textAlign: TextAlign.left,
        textAlignVertical: TextAlignVertical.top,
        keyboardType: TextInputType.multiline,
        textInputAction: TextInputAction.done,
        decoration: const InputDecoration(
          labelText: 'Comments',
          alignLabelWithHint: true,
          // hintText: 'Add comments here...',
          border: OutlineInputBorder(),
        ),
      ),
    );
  }
}

class PressureField extends StatelessWidget {
  final TextEditingController controller;
  const PressureField({
    super.key,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      keyboardType: const TextInputType.numberWithOptions(decimal: true),
      textInputAction: TextInputAction.next,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9.]'))
      ],
      decoration: const InputDecoration(
        labelText: 'System Pressure (PSI)',
        hintText: 'PSI',
        border: OutlineInputBorder(),
        suffixIcon: Icon(Icons.tire_repair_outlined),
      ),
      validator: validateOptional,
    );
  }
}

class PowerField extends StatelessWidget {
  final TextEditingController controller;
  const PowerField({
    super.key,
    required this.controller,
  });

  String? validatePower(String? value) {
    if (value == null || value.isEmpty) {
      return 'Please enter a value for the power field';
    } else {
      final numeric = int.tryParse(value);
      if (numeric == null) {
        return 'Please enter a valid number';
      }
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      keyboardType: const TextInputType.numberWithOptions(decimal: true),
      textInputAction: TextInputAction.next,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
      ],
      decoration: const InputDecoration(
        labelText: 'Electric Utility Meter Reading (KWH)',
        hintText: 'KWH',
        border: OutlineInputBorder(),
        suffixIcon: Icon(Icons.speed_outlined),
      ),
      validator: validatePower,
    );
  }
}

class Pump1RuntimeField extends StatelessWidget {
  final TextEditingController controller;

  const Pump1RuntimeField({
    super.key,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      keyboardType: const TextInputType.numberWithOptions(decimal: true),
      textInputAction: TextInputAction.next,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9.]'))
      ],
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Pump 1 Runtime (Hrs)',
        suffixIcon: Icon(Icons.timer_outlined),
      ),
      validator: validateOptional,
    );
  }
}

class Pump2RuntimeField extends StatelessWidget {
  final TextEditingController controller;

  const Pump2RuntimeField({
    super.key,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      keyboardType: const TextInputType.numberWithOptions(decimal: true),
      textInputAction: TextInputAction.next,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9.]'))
      ],
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Pump 2 Runtime (Hrs)',
        suffixIcon: Icon(Icons.timer_outlined),
      ),
      validator: validateOptional,
    );
  }
}

class StationField extends StatefulWidget {
  final List<String> stations;
  final ValueChanged<String?>? onValueChanged; // Callback function

  const StationField({
    Key? key,
    required this.stations,
    required this.onValueChanged,
  }) : super(key: key);

  @override
  State<StationField> createState() => StationFieldState();
}

class StationFieldState extends State<StationField> {
  String? selectedValue;
  bool isEdited = false;
  void reset() {
    setState(() {
      if (isEdited) {
        selectedValue = null;
        isEdited = false;
        FocusScope.of(context).unfocus();
      }
    });
  }

  String? validator(String? value) {
    if (value == null) {
      return 'Please select a station';
    }
    return null;
  }

  @override
  void initState() {
    super.initState();
    selectedValue = null;
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<String>(
      items: widget.stations.map((String station) {
        return DropdownMenuItem<String>(
          value: station,
          child: Text(station),
        );
      }).toList(),
      validator: validator,
      onChanged: (String? newValue) {
        setState(() {
          isEdited = true;
          selectedValue = newValue;
          if (newValue != null) {
            widget.onValueChanged?.call(newValue);
          }
        });
      },
      value: selectedValue,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Select a station *',
      ),
    );
  }
}

class DateInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    final text = _formatDate(newValue.text);
    return newValue.copyWith(
      text: text,
      selection: TextSelection.collapsed(offset: text.length),
    );
  }

  String _formatDate(String value) {
    value = value.replaceAll(RegExp(r'[^0-9]'), ''); // Remove non-digits
    final buffer = StringBuffer();
    for (var i = 0; i < value.length; i++) {
      if (i == 2 || i == 4) {
        buffer.write('/'); // Add slashes at the appropriate positions
      }
      buffer.write(value[i]);
    }
    return buffer.toString();
  }
}

class DateField extends StatefulWidget {
  final TextEditingController controller;
  final VoidCallback? onDateSelected; // Callback function

  const DateField(
      {super.key,
      required this.labelText,
      required this.controller,
      this.onDateSelected});
  final String labelText;

  @override
  State<DateField> createState() => DateFieldState();
}

class DateFieldState extends State<DateField> {
  late final TextEditingController _controller;
  String? _errorText;
  bool isValid = false;

  String? validateDate(String? value) {
    debugPrint("validateDate called with value: $value");

    if (value == null || value.isEmpty) {
      return 'Date is required';
    }
    if (!_validateDate(value)) {
      return 'Invalid date format';
    }
    return null;
  }

  @override
  void initState() {
    _controller = widget.controller;
    DateTime date = DateTime.now();

    _controller.text =
        '${date.month.toString().padLeft(2, '0')}/${date.day.toString().padLeft(2, '0')}/${date.year.toString()}';
    super.initState();
  }

  bool _validateDate(String date) {
    final parts = date.split('/');
    if (parts.length != 3) {
      return false; // Invalid format
    }
    final month = int.tryParse(parts[0]);
    final day = int.tryParse(parts[1]);
    if (month == null || day == null) {
      return false; // Non-numeric parts
    }
    if (month < 1 || month > 12) {
      return false; // Month out of range
    }
    if (day < 1 || day > _daysInMonth(month)) {
      return false; // Day out of range for the given month
    }
    return true; // Valid date
  }

  int _daysInMonth(int month) {
    if (month == 2) {
      // February (could add leap year logic here)
      return 28;
    } else if ([4, 6, 9, 11].contains(month)) {
      // Months with 30 days
      return 30;
    } else {
      // Months with 31 days
      return 31;
    }
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: _controller,
      keyboardType: TextInputType.datetime,
      textInputAction: TextInputAction.next,
      onTap: () async {
        DateTime? pickedDate = await showDatePicker(
              context: context,
              initialDate: DateTime.now(),
              firstDate: DateTime(2016),
              lastDate: DateTime.now(),
            ) ??
            DateTime.now();

        _controller.text =
            '${pickedDate.month.toString().padLeft(2, '0')}/${pickedDate.day.toString().padLeft(2, '0')}/${pickedDate.year.toString()}';
        isValid = true;
      },
      onChanged: (value) {
        _controller.text = value;
        _errorText = validateDate(value);
        isValid = _errorText == null;

        if (kDebugMode) {
          print(_controller.text);
        }
      },
      validator: validateDate,
      inputFormatters: [
        FilteringTextInputFormatter.digitsOnly,
        LengthLimitingTextInputFormatter(
            8), // Limit to 8 characters (mm/dd/yyyy)
        DateInputFormatter(), // Custom date input formatter
      ],
      decoration: InputDecoration(
        labelText: widget.labelText,
        hintText: 'mm/dd/yyyy',
        errorText: _errorText,
        border: const OutlineInputBorder(),
        suffixIcon: IconButton(
          icon: const Icon(Icons.calendar_today_outlined),
          onPressed: () async {
            DateTime? pickedDate = await showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime(2016),
                  lastDate: DateTime.now(),
                ) ??
                DateTime.now();

            _controller.text =
                '${pickedDate.month.toString().padLeft(2, '0')}/${pickedDate.day.toString().padLeft(2, '0')}/${pickedDate.year.toString()}';
            isValid = true;
          },
        ),
      ),
    );
  }
}

class TimeField extends StatefulWidget {
  final TextEditingController controller;

  const TimeField({
    super.key,
    required this.controller,
  });

  @override
  State<TimeField> createState() => _TimeFieldState();
}

class _TimeFieldState extends State<TimeField> {
  late final TextEditingController _controller;
  String? _errorText;
  bool _hasError = false;

  bool _validateTime(String time) {
    if (time.isEmpty) {
      return true; // Allow empty input
    }
    final parts = time.split(':');
    if (parts.length != 2) {
      return false; // Invalid format
    }

    final hour = int.tryParse(parts[0]);
    final minute = int.tryParse(parts[1]);

    if (hour == null || minute == null) {
      return false; // Non-numeric parts
    }

    if (hour < 0 || hour > 23 || minute < 0 || minute > 59) {
      return false; // Hour or minute out of range
    }

    return true; // Valid time
  }

  @override
  void initState() {
    _controller = widget.controller;

    final hour = DateTime.now().hour.toString().padLeft(2, '0'); // Format hour
    final minute =
        DateTime.now().minute.toString().padLeft(2, '0'); // Format minute
    _controller.text = '$hour:$minute';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: _controller,
      keyboardType: TextInputType.datetime,
      textInputAction: TextInputAction.next,
      onTap: () async {
        TimeOfDay pickedTime = await showTimePicker(
              context: context,
              initialTime: TimeOfDay.now(),
            ) ??
            TimeOfDay.now();
        setState(() {
          _controller.text =
              '${pickedTime.hour.toString().padLeft(2, '0')}:${pickedTime.minute.toString().padLeft(2, '0')}';
          if (kDebugMode) {
            print(_controller.text);
          }
        });
      },
      onChanged: (value) {
        setState(() {
          Form.maybeOf(context)?.validate();
          final isValid = _validateTime(value);
          _controller.text = value;
          _hasError = !isValid;
          _errorText = isValid ? null : 'Invalid time format';
          if (kDebugMode) {
            print(_controller.text);
          }
        });
      },
      decoration: InputDecoration(
        labelText: 'Time',
        hintText: 'hh:mm',
        errorText: _hasError ? _errorText : null,
        border: const OutlineInputBorder(),
        suffixIcon: IconButton(
          icon: const Icon(Icons.schedule_outlined),
          onPressed: () async {
            TimeOfDay pickedTime = await showTimePicker(
                  context: context,
                  initialTime: TimeOfDay.now(),
                ) ??
                TimeOfDay.now();
            setState(() {
              _controller.text =
                  '${pickedTime.hour.toString().padLeft(2, '0')}:${pickedTime.minute.toString().padLeft(2, '0')}';
              if (kDebugMode) {
                print(_controller.text);
              }
            });
          },
        ),
      ),
    );
  }
}
