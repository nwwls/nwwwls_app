import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../logic/cubit/theme_cubit.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(Icons.settings),
            const SizedBox(width: 8.0),
            Text(
              'Settings',
              style: Theme.of(context).textTheme.titleLarge,
            )
          ],
        ),
      ),
      body: ListView(
        children: [
          BlocBuilder<ThemeCubit, ThemeState>(
            builder: (context, state) {
              return SwitchListTile(
                value: state.themeMode == ThemeMode.dark,
                thumbIcon: state.themeMode == ThemeMode.dark
                    ? const MaterialStatePropertyAll(Icon(Icons.dark_mode))
                    : const MaterialStatePropertyAll(Icon(Icons.light_mode)),
                onChanged: ((value) {
                  context.read<ThemeCubit>().changeTheme();
                }),
                title: const Text('App Theme'),
              );
            },
          ),
          ListTile(
            leading: const Icon(Icons.lock_reset),
            title: const Text('Change Password'),
            trailing: const Icon(Icons.chevron_right),
            onTap: () => Navigator.of(context).pushNamed('/new-password'),
          ),
          ListTile(
            leading: const Icon(Icons.logout),
            title: const Text('Log Out'),
            trailing: const Icon(Icons.chevron_right),
            onTap: () => Navigator.of(context).pushReplacementNamed('/login'),
          ),
        ],
      ),
    );
  }
}
