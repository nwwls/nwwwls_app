import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'authentication/presentation/screens/login_screen.dart';
import 'core/logic/cubit/theme_cubit.dart';
import 'core/router/app_router.dart';
import 'measurement/data/providers/measurement_provider.dart';
import 'measurement/data/repositories/measurement_repository.dart';
import 'measurement/logic/bloc/measurement_bloc.dart';

import 'core/themes/material_theme/color_schemes.g.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  sqfliteFfiInit(); // Initialize sqflite_ffi
  // databaseFactory = databaseFactoryFfi;
  final provider = MeasurementProvider();
  final repository = MeasurementRepository(provider);
  runApp(MultiBlocProvider(providers: [
    BlocProvider(create: ((context) => ThemeCubit())),
    BlocProvider(create: (((context) => MeasurementBloc(repository)))),
  ], child: const MainApp()));
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State<MainApp> createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  final AppRouter _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ThemeCubit, ThemeState>(
      builder: (context, state) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
              useMaterial3: true,
              colorScheme: lightColorScheme,
              visualDensity: VisualDensity.adaptivePlatformDensity),
          darkTheme: ThemeData(
              useMaterial3: true,
              colorScheme: darkColorScheme,
              visualDensity: VisualDensity.adaptivePlatformDensity),
          themeMode: context.read<ThemeCubit>().state.themeMode,
          onGenerateRoute: _appRouter.onGenerateRoute,
          home: Scaffold(
            body: BlocProvider.value(
              value: BlocProvider.of<ThemeCubit>(context),
              child: const LoginScreen(),
            ),
          ),
        );
      },
    );
  }
}
