// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/widgets/input_fields.dart';
import '../../data/models/measurement.dart';
import '../../logic/bloc/measurement_bloc.dart';
import '../widgets/view_data_bar_chart.dart';
import '../widgets/view_data_line_chart.dart';
import '../widgets/view_data_table.dart';

class ViewDataScreen extends StatelessWidget {
  const ViewDataScreen({
    Key? key,
    required this.measurements,
    required this.fromDate,
    required this.toDate,
  }) : super(key: key);
  final List<Measurement> measurements;
  final DateTime fromDate;
  final DateTime toDate;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('View Data'),
          bottom: const TabBar(
            tabs: [
              Tab(
                icon: Icon(Icons.bar_chart_outlined),
                text: 'Graph View',
                height: 56,
              ),
              Tab(
                icon: Icon(Icons.table_chart_outlined),
                text: 'Table View',
                height: 56,
              ),
            ],
          ),
        ),
        // bottomNavigationBar: const AppNavigationBar(),
        body: SafeArea(
          bottom: true,
          child: TabBarView(
            children: [
              SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      BlocBuilder<MeasurementBloc, MeasurementState>(
                        builder: (context, state) {
                          if (state is MeasurementsLoaded) {
                            return Column(
                              children: [
                                Text(
                                  '${state.measurements[0].stationName} - ID',
                                  style:
                                      Theme.of(context).textTheme.headlineSmall,
                                ),
                                const SizedBox(height: 4),
                                Text(
                                    '${convertDateFormat(fromDate.toIso8601String().substring(0, 10))}  to: ${convertDateFormat(toDate.toIso8601String().substring(0, 10))}'),
                              ],
                            );
                          } else {
                            return const Text('Failed to load station data');
                          }
                        },
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.4,
                        child: PowerBarChart(
                          fromDate: fromDate,
                          toDate: toDate,
                          measurementData: measurements,
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.4,
                        child: RuntimeLineChart(
                          fromDate: fromDate,
                          toDate: toDate,
                          measurementData: measurements,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(24.0),
                  child: Column(
                    children: [
                      BlocBuilder<MeasurementBloc, MeasurementState>(
                        builder: (context, state) {
                          if (state is MeasurementsLoaded) {
                            return Column(
                              children: [
                                Text(
                                  '${state.measurements[0].stationName} – #ID',
                                  style:
                                      Theme.of(context).textTheme.headlineSmall,
                                ),
                                const SizedBox(height: 4),
                                Text(
                                    '${convertDateFormat(fromDate.toIso8601String().substring(0, 10))}  to: ${convertDateFormat(toDate.toIso8601String().substring(0, 10))}'),
                              ],
                            );
                          } else {
                            return const Text('Failed to load station data');
                          }
                        },
                      ),
                      const SizedBox(height: 12.0),
                      BlocBuilder<MeasurementBloc, MeasurementState>(
                        builder: (context, state) {
                          if (state is MeasurementsLoaded) {
                            return SingleChildScrollView(
                              child: TableView(
                                measurementData: state.measurements,
                              ),
                            );
                          } else {
                            return const Card(
                              child: Center(
                                child: Text('Measurements not loaded...'),
                              ),
                            );
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
