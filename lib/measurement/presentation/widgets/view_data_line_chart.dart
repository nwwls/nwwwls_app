// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../data/models/measurement.dart';
import '../../logic/bloc/measurement_bloc.dart';

class RuntimeLineChart extends StatelessWidget {
  const RuntimeLineChart({
    Key? key,
    required this.measurementData,
    required this.fromDate,
    required this.toDate,
  }) : super(key: key);
  final List<Measurement> measurementData;
  final DateTime fromDate;
  final DateTime toDate;

  @override
  Widget build(BuildContext context) {
    return Container(
      transform: Matrix4.translationValues(0, -64, 0),
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              transform: Matrix4.translationValues(0, 40, 0),
              child: Text(
                'Pump Runtime (Hrs)',
                style: Theme.of(context).textTheme.titleMedium,
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: BlocBuilder<MeasurementBloc, MeasurementState>(
                builder: (context, state) {
                  if (state is MeasurementsLoaded) {
                    final points = fromDate.difference(toDate).inDays.abs() + 1;
                    return SizedBox(
                      width: points * 44,
                      child: SfCartesianChart(
                        primaryXAxis: CategoryAxis(),
                        primaryYAxis: NumericAxis(isVisible: true),
                        tooltipBehavior: TooltipBehavior(
                          enable: true,
                          color: Theme.of(context).colorScheme.inverseSurface,
                          builder:
                              (data, point, series, pointIndex, seriesIndex) =>
                                  Container(
                            decoration: BoxDecoration(
                                color: Theme.of(context)
                                    .colorScheme
                                    .inverseSurface,
                                borderRadius: BorderRadius.circular(4.0)),
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Runtime: ${point.y.toStringAsFixed(1)} Hrs.',
                              style: TextStyle(
                                  color: Theme.of(context)
                                      .colorScheme
                                      .onInverseSurface),
                            ),
                          ),
                        ),
                        series: <ChartSeries>[
                          FastLineSeries<PumpData, String>(
                            emptyPointSettings:
                                EmptyPointSettings(mode: EmptyPointMode.drop),
                            dataSource: getLineData(
                                state.measurements, fromDate, toDate),
                            xValueMapper: (PumpData pump, _) => pump.date,
                            yValueMapper: (PumpData pump, _) =>
                                pump.pump1Runtime,
                            markerSettings: const MarkerSettings(
                              isVisible: true,
                            ),
                          ),
                          FastLineSeries<PumpData, String>(
                            emptyPointSettings:
                                EmptyPointSettings(mode: EmptyPointMode.drop),
                            dataSource: getLineData(
                                state.measurements, fromDate, toDate),
                            xValueMapper: (PumpData pump, _) => pump.date,
                            yValueMapper: (PumpData pump, _) =>
                                pump.pump2Runtime,
                            markerSettings: const MarkerSettings(
                              isVisible: true,
                            ),
                          )
                        ],
                      ),
                    );
                  } else {
                    return const Card(
                      child: Center(
                        child: Text('Measurements not loaded...'),
                      ),
                    );
                  }
                },
              ),
            ),
            const RuntimeChartLegend(),
          ],
        ),
      ),
    );
  }
}

class RuntimeChartLegend extends StatelessWidget {
  const RuntimeChartLegend({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          children: [
            Icon(
              Icons.circle,
              size: 12.0,
              color: Colors.blue.shade700,
            ),
            const SizedBox(width: 12),
            const Text('Pump 1 Runtime')
          ],
        ),
        const SizedBox(width: 32),
        Row(
          children: [
            Icon(
              Icons.circle,
              size: 12.0,
              color: Colors.pink.shade700,
            ),
            const SizedBox(width: 12),
            const Text('Pump 2 Runtime')
          ],
        ),
      ],
    );
  }
}

class PumpData {
  final String date;
  final num? pump1Runtime;
  final num? pump2Runtime;

  const PumpData({
    required this.date,
    required this.pump1Runtime,
    required this.pump2Runtime,
  });
}

dynamic getLineData(List<Measurement> measurements, fromDate, toDate) {
  measurements.sort((a, b) => a.date.compareTo(b.date));

  // Get the start and end dates
  final DateTime startDate = fromDate;
  final DateTime endDate = toDate;

  // Create a map to store data for each date
  final Map<String, PumpData> dataMap = {};

  // Initialize the map with zero power values for all dates in the range

  for (DateTime date = startDate;
      date.isBefore(endDate);
      date = date.add(const Duration(days: 1))) {
    final String formattedDate =
        '${date.month.toString().padLeft(2, '0')}/${date.day.toString().padLeft(2, '0')}';
    dataMap[formattedDate] =
        PumpData(date: formattedDate, pump1Runtime: null, pump2Runtime: null);
  }

  // Populate the map with actual data from measurements
  for (var i = 0; i < measurements.length; i++) {
    final String formattedDate =
        '${measurements[i].date.month.toString().padLeft(2, '0')}/${measurements[i].date.day.toString().padLeft(2, '0')}';
    dataMap[formattedDate] = PumpData(
      pump1Runtime: measurements[i].p1Runtime,
      date: formattedDate,
      pump2Runtime: measurements[i].p2Runtime,
    );
  }
  // Convert the map values to a list
  final List<PumpData> lineData = dataMap.values.toList();

  if (kDebugMode) {
    print(lineData.length);
  }
  return lineData;
}
