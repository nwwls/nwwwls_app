// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../data/models/measurement.dart';
import '../../logic/bloc/measurement_bloc.dart';

class PowerBarChart extends StatefulWidget {
  const PowerBarChart({
    Key? key,
    required this.measurementData,
    required this.fromDate,
    required this.toDate,
  }) : super(key: key);
  final List<Measurement> measurementData;
  final DateTime fromDate;
  final DateTime toDate;

  @override
  State<PowerBarChart> createState() => _PowerBarChartState();
}

class _PowerBarChartState extends State<PowerBarChart> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Container(
        clipBehavior: Clip.none,
        transform: Matrix4.translationValues(0, -20, 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              transform: Matrix4.translationValues(0, 40, 0),
              child: Text(
                'Energy Usage (KWH/D)',
                style: Theme.of(context).textTheme.titleMedium,
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: BlocBuilder<MeasurementBloc, MeasurementState>(
                builder: (context, state) {
                  if (state is MeasurementsLoaded) {
                    final points =
                        widget.fromDate.difference(widget.toDate).inDays.abs() +
                            1;
                    final measurements = state.measurements;
                    final graphData = getBarsData(
                        measurements, widget.fromDate, widget.toDate);

                    return SizedBox(
                      width: points * 44,
                      child: SfCartesianChart(
                        margin: const EdgeInsets.all(12.0),
                        // zoomPanBehavior: _zoomPanBehavior,
                        primaryXAxis: CategoryAxis(),
                        primaryYAxis: NumericAxis(isVisible: false),
                        tooltipBehavior: TooltipBehavior(
                            enable: true,
                            color: Theme.of(context).colorScheme.inverseSurface,
                            builder:
                                (data, point, series, pointIndex, seriesIndex) {
                              final String pressureText = graphData[pointIndex]
                                          .pressure !=
                                      null
                                  ? 'Pressure: ${graphData[pointIndex].pressure?.toStringAsFixed(1)} PSI'
                                  : 'Pressure: N/A';
                              return Container(
                                decoration: BoxDecoration(
                                    color: Theme.of(context)
                                        .colorScheme
                                        .inverseSurface,
                                    borderRadius: BorderRadius.circular(4.0)),
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  pressureText,
                                  style: TextStyle(
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onInverseSurface,
                                  ),
                                ),
                              );
                            }),
                        series: <ChartSeries>[
                          ColumnSeries<PowerData, String>(
                            emptyPointSettings:
                                EmptyPointSettings(mode: EmptyPointMode.drop),
                            dataSource: graphData,
                            xValueMapper: (PowerData powerData, _) =>
                                powerData.date,
                            yValueMapper: (PowerData powerData, _) =>
                                powerData.power,
                            color: Colors.blue,
                            width: 1,
                            spacing: 0.25,
                            borderRadius: BorderRadius.circular(4),
                            dataLabelMapper: (datum, index) =>
                                (datum.power?.round()).toString(),
                            dataLabelSettings: const DataLabelSettings(
                              // alignment: ChartAlignment.far,
                              labelAlignment: ChartDataLabelAlignment.top,
                              isVisible: true,
                              labelPosition: ChartDataLabelPosition.outside,
                            ),
                          ),
                        ],
                      ),
                    );
                  } else {
                    return const Card(
                      child: Center(child: Text('Measurements not loaded...')),
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class PowerData {
  final String date;
  final num? power;
  final num? pressure;

  PowerData(this.pressure, {required this.date, required this.power});

  @override
  String toString() =>
      'PowerData(date: $date, power: $power, pressure: $pressure)';
}

dynamic getBarsData(List<Measurement?> measurements, fromDate, toDate) {
  measurements.sort((a, b) => a!.date.compareTo(b!.date));

  // Get the start and end dates
  final DateTime startDate = fromDate;
  final DateTime endDate = toDate;

  // Create a map to store data for each date
  final Map<String, PowerData> dataMap = {};

  // Initialize the map with zero power values for all dates in the range

  for (DateTime date = startDate;
      date.isBefore(endDate);
      date = date.add(const Duration(days: 1))) {
    final String formattedDate =
        '${date.month.toString().padLeft(2, '0')}/${date.day.toString().padLeft(2, '0')}';
    dataMap[formattedDate] = PowerData(null, date: formattedDate, power: null);
  }

  // Populate the map with actual data from measurements
  for (var i = 0; i < measurements.length; i++) {
    final String formattedDate =
        '${measurements[i]!.date.month.toString().padLeft(2, '0')}/${measurements[i]!.date.day.toString().padLeft(2, '0')}';
    dataMap[formattedDate] = PowerData(
      measurements[i]!.pressure,
      date: formattedDate,
      power: measurements[i]!.powerMeter,
    );
  }

  // Convert the map values to a list
  final List<PowerData> barsData = dataMap.values.toList();

  return barsData;
}
