import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nwwwls_app/measurement/logic/bloc/measurement_bloc.dart';

import '../../../core/widgets/input_fields.dart';
import '../../data/models/measurement.dart';

TextEditingController dateController = TextEditingController();
TextEditingController timeController = TextEditingController();
TextEditingController powerController = TextEditingController();
TextEditingController pump1Controller = TextEditingController();
TextEditingController pump2Controller = TextEditingController();
TextEditingController pressureController = TextEditingController();
TextEditingController commentController = TextEditingController();
bool powerUpdated = false;
final GlobalKey<DateFieldState> dateFieldKey = GlobalKey<DateFieldState>();
final stationFieldKey = GlobalKey<StationFieldState>();

class NewDataForm extends StatefulWidget {
  const NewDataForm({super.key});

  @override
  State<NewDataForm> createState() => _NewDataFormState();
}

Measurement measurementData =
    Measurement.empty().copyWith(stationName: stationList[0]);
late String stationValue;
final List<String> stationList = [
  'St Cathrine Creek',
  'Azalea Gardens',
  'Beau Pre',
  'Cottage Farm',
  'Covington Apt',
  'Daisy St',
  'Days Inn',
  'Hampton Ct',
  'Ivy Lane',
  'MLK',
  'Mt Carmel',
  'Oakland #1',
  'Pintard',
  'Port',
  'Roth Hill',
  'Roux 61',
  'Silver St'
];

class _NewDataFormState extends State<NewDataForm> {
  final formKey = GlobalKey<FormState>();
  bool _isSubmitted = false;

  void updateFormFieldsWithMeasurement(Measurement measurement) {
    setState(() {
      stationValue = measurement.stationName;
      dateController.text = convertDateFormat(
          measurement.date.toIso8601String().substring(0, 10));
      timeController.text = measurement.time != null
          ? '${measurement.time!.hour.toString().padLeft(2, '0')}:${measurement.time!.minute.toString().padLeft(2, '0')}'
          : '';
      powerController.text = measurement.powerMeter.toString();
      pump1Controller.text =
          measurement.p1Runtime != null ? measurement.p1Runtime.toString() : '';
      pump2Controller.text =
          measurement.p2Runtime != null ? measurement.p2Runtime.toString() : '';
      pressureController.text =
          measurement.pressure != null ? measurement.pressure.toString() : '';
      commentController.text = measurement.comment ?? '';
    });
  }

  _showAlert(BuildContext context) {
    return AlertDialog(
      title: const Text('Incomplete data'),
      actions: [
        TextButton(
            onPressed: Navigator.of(context).pop, child: const Text('Ok'))
      ],
    );
  }

  _createSucessSnackBar(BuildContext context) {
    return const SnackBar(content: Text('Measurement created succesfully'));
  }

  _noDataSnackBar(BuildContext context) {
    return const SnackBar(
        content: Text('Couldn\' find any data for the given date'));
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Builder(builder: (context) {
        return Form(
          autovalidateMode: AutovalidateMode.always,
          key: formKey,
          child: Builder(builder: (context) {
            return FocusTraversalGroup(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  FocusTraversalOrder(
                      order: const NumericFocusOrder(0),
                      child: StationField(
                        key: stationFieldKey,
                        stations: stationList,
                        onValueChanged: (value) {
                          debugPrint(value);
                          measurementData.stationName = value!;
                        },
                      )),
                  const SizedBox(height: 12.0),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: FocusTraversalOrder(
                          order: const NumericFocusOrder(1),
                          child: Form(
                            child: DateField(
                              key: dateFieldKey,
                              controller: dateController,
                              labelText: 'Date *',
                              onDateSelected: () {
                                // Trigger form validation
                                if (dateFieldKey.currentState != null) {
                                  if (kDebugMode) {
                                    print(dateFieldKey.currentState);
                                  }
                                  dateFieldKey.currentState!
                                      .validateDate(dateController.text);
                                }
                              },
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 4.0),
                      Expanded(
                          child: FocusTraversalOrder(
                        order: const NumericFocusOrder(2),
                        child: TimeField(
                          controller: timeController,
                        ),
                      ))
                    ],
                  ),
                  const SizedBox(height: 12.0),
                  FocusTraversalOrder(
                    order: const NumericFocusOrder(3),
                    child: PowerField(
                      controller: powerController,
                    ),
                  ),
                  const SizedBox(height: 12.0),
                  Row(
                    children: [
                      Expanded(
                          child: FocusTraversalOrder(
                        order: const NumericFocusOrder(4),
                        child: Pump1RuntimeField(
                          controller: pump1Controller,
                        ),
                      )),
                      const SizedBox(width: 4.0),
                      Expanded(
                          child: FocusTraversalOrder(
                        order: const NumericFocusOrder(5),
                        child: Pump2RuntimeField(
                          controller: pump2Controller,
                        ),
                      )),
                    ],
                  ),
                  const SizedBox(height: 12.0),
                  FocusTraversalOrder(
                    order: const NumericFocusOrder(6),
                    child: PressureField(
                      controller: pressureController,
                    ),
                  ),
                  const SizedBox(height: 12.0),
                  // ignore: void_checks
                  FocusTraversalOrder(
                    order: const NumericFocusOrder(7),
                    child: CommentsField(
                      controller: commentController,
                    ),
                  ),
                  const SizedBox(height: 12.0),
                  Row(
                    children: [
                      BlocListener<MeasurementBloc, MeasurementState>(
                        listener: (context, state) {
                          if (state is MeasurementCreateOrUpdateInProgress) {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                content: Row(
                              children: [
                                SizedBox(
                                  width: 12,
                                  height: 12,
                                  child: CircularProgressIndicator(
                                    strokeWidth: 2,
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onInverseSurface,
                                  ),
                                ),
                                const SizedBox(width: 8),
                                const Text('Creating measurement...'),
                              ],
                            )));
                          } else if (state
                              is MeasurementCreateOrUpdateSuccess) {
                            ScaffoldMessenger.of(context).hideCurrentSnackBar();
                            ScaffoldMessenger.of(context)
                                .showSnackBar(_createSucessSnackBar(context));
                          } else if (state is MeasurementError) {
                            ScaffoldMessenger.of(context).hideCurrentSnackBar();
                            ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(content: Text(state.errorMessage)));
                          } else if (state is MeasurementEmpty) {
                            final String stationName =
                                measurementData.stationName;
                            measurementData = Measurement.empty()
                                .copyWith(stationName: stationName);
                            updateFormFieldsWithMeasurement(measurementData);
                            powerController.clear();
                            timeController.clear();
                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(
                                content: Text('No measurements found...'),
                                duration: Duration(seconds: 2),
                              ),
                            );
                          } else if (state is MeasurementLoaded) {
                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(
                                content: Text('Measurement loaded...'),
                                duration: Duration(seconds: 1),
                              ),
                            );
                            updateFormFieldsWithMeasurement(state.measurement);
                          }
                        },
                        child: Expanded(
                          child: FilledButton.icon(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Theme.of(context)
                                      .colorScheme
                                      .tertiaryContainer),
                              foregroundColor: MaterialStateProperty.all<Color>(
                                  Theme.of(context)
                                      .colorScheme
                                      .onTertiaryContainer),
                              shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12.0),
                                ),
                              ),
                            ),
                            onPressed: () async {
                              if (formKey.currentState!.validate()) {
                                measurementData.date =
                                    parseDate(dateController.text);
                                measurementData.time =
                                    parseTime(timeController.text);
                                measurementData.powerMeter =
                                    num.parse(powerController.text);
                                measurementData.p1Runtime =
                                    num.tryParse(pump1Controller.text);
                                measurementData.p2Runtime =
                                    num.tryParse(pump2Controller.text);
                                measurementData.pressure =
                                    num.tryParse(pressureController.text);
                                measurementData.comment =
                                    commentController.text;
                                formKey.currentState!.save();

                                context.read<MeasurementBloc>().add(
                                    CreateOrUpdateMeasurementEvent(
                                        measurementData));
                              }
                            },
                            icon:
                                const Icon(Icons.assignment_turned_in_outlined),
                            label: const Text('SAVE'),
                          ),
                        ),
                      ),
                      const SizedBox(width: 8.0),
                      Expanded(
                        child: FilledButton.icon(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Theme.of(context).colorScheme.errorContainer),
                            foregroundColor: MaterialStateProperty.all<Color>(
                                Theme.of(context).colorScheme.onErrorContainer),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12.0),
                              ),
                            ),
                          ),
                          onPressed: () {
                            formKey.currentState!.reset();
                            setState(() {
                              measurementData = Measurement.empty()
                                  .copyWith(stationName: stationList[0]);
                              debugPrint(measurementData.toString());
                              // dateController.clear();
                              timeController.clear();
                              powerController.clear();
                              pump1Controller.clear();
                              pump2Controller.clear();
                              pressureController.clear();
                              commentController.clear();
                              stationFieldKey.currentState!.reset();
                              debugPrint(dateController.text);
                            });
                          },
                          icon: const Icon(Icons.backspace_outlined),
                          label: const Text('CLEAR'),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 12.0),
                  SizedBox(
                    width: double.infinity,
                    child: FilledButton.icon(
                      style: ButtonStyle(
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                        ),
                      ),
                      onPressed: () async {
                        final isStationValid =
                            measurementData.stationName.isNotEmpty;
                        if (isStationValid && dateController.text != '') {
                          final selectedStation = measurementData.stationName;
                          final selectedDate = parseDate(dateController.text);
                          if (kDebugMode) {
                            print('fetching measurement...');
                          }
                          context.read<MeasurementBloc>().add(
                                FetchMeasurementEvent(
                                    stationName: selectedStation,
                                    date: selectedDate),
                              );
                        }
                      },
                      icon: const Icon(Icons.assignment_returned_outlined),
                      label: const Text('RETRIEVE DATA'),
                    ),
                  ),
                ],
              ),
            );
          }),
        );
      }),
    );
  }
}
