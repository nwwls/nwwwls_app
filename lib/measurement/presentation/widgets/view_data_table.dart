import 'package:flutter/material.dart';

import '../../data/models/measurement.dart';

class TableView extends StatelessWidget {
  const TableView({super.key, required this.measurementData});
  final List<Measurement> measurementData;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: DataTable(
        columns: const [
          DataColumn(label: Text('Date')),
          DataColumn(label: Text('Energy Usage (KWH/D)'), numeric: true),
          DataColumn(label: Text('Pressure (PSI)'), numeric: true),
          DataColumn(label: Text('P1 RT (Hrs)'), numeric: true),
          DataColumn(label: Text('P2 RT (Hrs)'), numeric: true),
          DataColumn(label: Text('Comments')),
        ],
        rows: getRows(),
      ),
    );
  }

  List<DataRow> getRows() {
    List<DataRow> rows = [];
    for (var i = 0; i < measurementData.length; i++) {
      final item = measurementData[i];
      final day = item.date.day;
      final month = item.date.month;
      final year = item.date.year;
      rows.add(
        DataRow(
          cells: [
            DataCell(Text('$month/$day/$year')),
            DataCell(Center(child: Text(item.powerMeter.toString()))),
            DataCell(Center(
              child: item.pressure != null
                  ? item.pressure!.floor() == item.pressure
                      ? Text(item.pressure!.toInt().toStringAsFixed(1))
                      : Text(item.pressure!.toStringAsFixed(1))
                  : const Text(''),
            )),
            DataCell(Center(
              child: item.p1Runtime != null
                  ? item.p1Runtime!.floor() == item.p1Runtime
                      ? Text(item.p1Runtime!.toInt().toStringAsFixed(1))
                      : Text(item.p1Runtime!.toStringAsFixed(1))
                  : const Text(''),
            )),
            DataCell(Center(
              child: item.p2Runtime != null
                  ? item.p2Runtime!.floor() == item.p2Runtime
                      ? Text(item.p2Runtime!.toInt().toStringAsFixed(1))
                      : Text(item.p2Runtime!.toStringAsFixed(1))
                  : const Text(''),
            )),
            DataCell(
              SingleChildScrollView(
                child:
                    SizedBox(width: 160, child: Text(item.comment.toString())),
              ),
            ),
          ],
        ),
      );
    }
    return rows;
  }
}
