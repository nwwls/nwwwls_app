import 'package:flutter/material.dart';

import '../../../core/widgets/input_fields.dart';
import '../../data/models/measurement.dart';

TextEditingController dateController = TextEditingController();
TextEditingController timeController = TextEditingController();
TextEditingController powerController = TextEditingController();
TextEditingController pump1Controller = TextEditingController();
TextEditingController pump2Controller = TextEditingController();
TextEditingController pressureController = TextEditingController();
TextEditingController commentController = TextEditingController();
bool powerUpdated = false;

class UpdateDataForm extends StatefulWidget {
  const UpdateDataForm({super.key});

  @override
  State<UpdateDataForm> createState() => _UpdateDataFormState();
}

Measurement measurementData = Measurement.empty();

final List<String> stationList = [
  'St Cathrine Creek',
  'Azalea Gardens',
  'Beau Pre',
  'Cottage Farm',
  'Covington Apt',
  'Daisy St',
  'Days Inn',
  'Hampton Ct',
  'Ivy Lane',
  'MLK',
  'Mt Carmel',
  'Oakland #1',
  'Pintard',
  'Port',
  'Roth Hill',
  'Roux 61',
  'Silver St'
];

class _UpdateDataFormState extends State<UpdateDataForm> {
  final formKey = GlobalKey<FormState>();

  _showAlert(BuildContext context) {
    return AlertDialog(
      title: const Text('Incomplete data'),
      actions: [
        TextButton(
            onPressed: Navigator.of(context).pop, child: const Text('Ok'))
      ],
    );
  }

  _noDataSnackBar(BuildContext context) {
    return const SnackBar(
        content: Text('Couldn\' find any data for the given date'));
  }

  void _populateFormFields(Measurement measurement) {
    setState(() {
      final date = measurement.date ?? DateTime.now();
      final time = measurement.time ?? TimeOfDay.now();
      dateController.text =
          '${date.month.toString().padLeft(2, '0')}/${date.day.toString().padLeft(2, '0')}/${date.year}';
      timeController.text =
          '${time.hour.toString().padLeft(2, '0')}:${time.minute.toString().padLeft(2, '0')}';
      powerController.text = measurement.powerMeter.toString();
      pump1Controller.text = measurement.p1Runtime?.toString() ?? '';
      pump2Controller.text = measurement.p2Runtime?.toString() ?? '';
      pressureController.text = measurement.pressure?.toString() ?? '';
      measurementData = measurement;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Builder(builder: (context) {
        return Form(
          key: formKey,
          child: Builder(builder: (context) {
            return FocusTraversalGroup(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  FocusTraversalOrder(
                      order: const NumericFocusOrder(0),
                      child: StationField(
                        stations: stationList,
                        onValueChanged: (value) => true,
                      )),
                  const SizedBox(height: 12.0),
                  Row(
                    children: [
                      Expanded(
                        child: FocusTraversalOrder(
                          order: const NumericFocusOrder(1),
                          child: DateField(
                            controller: dateController,
                            labelText: 'Date *',
                          ),
                        ),
                      ),
                      const SizedBox(width: 4.0),
                      Expanded(
                        child: FocusTraversalOrder(
                          order: const NumericFocusOrder(2),
                          child: TimeField(
                            controller: timeController,
                          ),
                        ),
                      )
                    ],
                  ),
                  const SizedBox(height: 12.0),
                  // FocusTraversalOrder(
                  //     order: const NumericFocusOrder(3),
                  //     child: PowerField(controller: powerController,isSubmitted: _,)),
                  const SizedBox(height: 12.0),
                  Row(
                    children: [
                      Expanded(
                        child: FocusTraversalOrder(
                          order: const NumericFocusOrder(4),
                          child: Pump1RuntimeField(
                            controller: pump1Controller,
                          ),
                        ),
                      ),
                      const SizedBox(width: 4.0),
                      Expanded(
                        child: FocusTraversalOrder(
                          order: const NumericFocusOrder(5),
                          child: Pump2RuntimeField(
                            controller: pump2Controller,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 12.0),
                  FocusTraversalOrder(
                    order: const NumericFocusOrder(6),
                    child: PressureField(
                      controller: powerController,
                    ),
                  ),
                  const SizedBox(height: 12.0),
                  FocusTraversalOrder(
                    order: const NumericFocusOrder(7),
                    child: CommentsField(
                      controller: commentController,
                    ),
                  ),
                  const SizedBox(height: 12.0),
                  Row(
                    children: [
                      Expanded(
                        child: FilledButton.icon(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                              Theme.of(context).colorScheme.tertiaryContainer,
                            ),
                            foregroundColor: MaterialStateProperty.all<Color>(
                              Theme.of(context).colorScheme.onTertiaryContainer,
                            ),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12.0),
                              ),
                            ),
                          ),
                          onPressed: () async {
                            if (formKey.currentState!.validate()) {
                              formKey.currentState!.save();
                            }
                          },
                          icon: const Icon(Icons.assignment_turned_in_outlined),
                          label: const Text('UPDATE'),
                        ),
                      ),
                      const SizedBox(width: 8.0),
                      Expanded(
                        child: FilledButton.icon(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Theme.of(context).colorScheme.errorContainer),
                            foregroundColor: MaterialStateProperty.all<Color>(
                                Theme.of(context).colorScheme.onErrorContainer),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12.0),
                              ),
                            ),
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: const Icon(Icons.backspace_outlined),
                          label: const Text('CANCEL'),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            );
          }),
        );
      }),
    );
  }
}
