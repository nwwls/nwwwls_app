import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nwwwls_app/core/widgets/input_fields.dart';
import 'package:nwwwls_app/measurement/presentation/widgets/new_data_form.dart';

import '../../logic/bloc/measurement_bloc.dart';
import '../screens/view_data_screen.dart';

final fromDateController = TextEditingController();
final toDateController = TextEditingController();

final List<String> stationList = [
  'St Cathrine Creek',
  'Azalea Gardens',
  'Beau Pre',
  'Cottage Farm',
  'Covington Apt',
  'Daisy St',
  'Days Inn',
  'Hampton Ct',
  'Ivy Lane',
  'MLK',
  'Mt Carmel',
  'Oakland #1',
  'Pintard',
  'Port',
  'Roth Hill',
  'Roux 61',
  'Silver St'
];

class ViewDataForm extends StatelessWidget {
  ViewDataForm({super.key});
  String stationName = stationList[0];
  late DateTime fromDate;
  late DateTime toDate;

  void _viewData(BuildContext context) async {
    final DateTime fromDate = DateTime.now();
    final DateTime toDate = DateTime.now();

    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => ViewDataScreen(
          measurements: [],
          fromDate: fromDate,
          toDate: toDate,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: FocusTraversalGroup(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FocusTraversalOrder(
              order: const NumericFocusOrder(0),
              child: StationField(
                stations: stationList,
                onValueChanged: (value) => stationName = value!,
              ),
            ),
            const SizedBox(height: 12.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(
                  child: FocusTraversalOrder(
                    order: const NumericFocusOrder(1),
                    child: DateField(
                      labelText: 'From',
                      controller: fromDateController,
                    ),
                  ),
                ),
                const SizedBox(width: 12.0),
                Expanded(
                    child: FocusTraversalOrder(
                  order: const NumericFocusOrder(2),
                  child: DateField(
                    labelText: 'To',
                    controller: toDateController,
                  ),
                )),
              ],
            ),
            const SizedBox(height: 12.0),
            const ViewDataChoice(),
            const SizedBox(height: 12.0),
            BlocListener<MeasurementBloc, MeasurementState>(
              listener: (context, state) {
                if (state is MeasurementsLoaded) {
                  final count = state.measurements.length;
                  final points = state.measurements[0]!.date
                          .difference(state.measurements.last!.date)
                          .inDays
                          .abs() +
                      1;
                  if (kDebugMode) {
                    for (var element in state.measurements) {
                      print(element!.date);
                      print(element.powerMeter);
                      print(element.p1Runtime);
                      print(element.p2Runtime);
                    }
                  }
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(
                          'Loaded $count measurements, show $points ponits on graph'),
                      duration: const Duration(seconds: 1),
                    ),
                  );
                }
              },
              child: FilledButton(
                // onPressed: () => Navigator.of(context).pushNamed('/view-data'),
                onPressed: () {
                  fromDate = DateTime(
                    int.parse(fromDateController.text.substring(6, 10)), // Year
                    int.parse(fromDateController.text.substring(0, 2)), // Month
                    int.parse(fromDateController.text.substring(3, 5)), // Day
                  );

                  toDate = DateTime(
                    int.parse(toDateController.text.substring(6, 10)), // Year
                    int.parse(toDateController.text.substring(0, 2)), // Month
                    int.parse(toDateController.text.substring(3, 5)), // Day
                  );
                  context.read<MeasurementBloc>().add(
                      FetchMeasurementsForStationInDateRangeEvent(
                          stationName,
                          parseDate(fromDateController.text),
                          parseDate(toDateController.text)));
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => ViewDataScreen(
                        measurements: [],
                        fromDate: fromDate,
                        toDate: toDate,
                      ),
                    ),
                  );
                  // _viewData(context);
                },
                child: const Text('View Data'),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
            ),
          ],
        ),
      ),
    );
  }
}

enum ViewType { graph, table }

class ViewDataChoice extends StatefulWidget {
  const ViewDataChoice({super.key});

  @override
  State<ViewDataChoice> createState() => _ViewDataChoiceState();
}

class _ViewDataChoiceState extends State<ViewDataChoice> {
  ViewType viewType = ViewType.graph;

  @override
  Widget build(BuildContext context) {
    return SegmentedButton<ViewType>(
      segments: const <ButtonSegment<ViewType>>[
        ButtonSegment<ViewType>(
            value: ViewType.graph,
            label: Text('Graph'),
            icon: Icon(Icons.bar_chart_outlined)),
        ButtonSegment<ViewType>(
            value: ViewType.table,
            label: Text('Table'),
            icon: Icon(Icons.table_chart_outlined)),
      ],
      selected: <ViewType>{viewType},
      onSelectionChanged: (Set<ViewType> newSelection) {
        setState(() {
          viewType = newSelection.first;
        });
      },
    );
  }
}
