import 'package:flutter/foundation.dart';
import 'package:path/path.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

import '../models/measurement.dart';

class MeasurementProvider {
  static const String mainTable = 'measurements';
  static const String tblDaily = 'tblDaily';
  static const String colDate = 'date';
  static const String colStationName = 'stationName';
  static const String colTime = 'time';
  static const String colPowerMeter = 'powerMeter';
  static const String colP1Runtime = 'p1Runtime';
  static const String colP2Runtime = 'p2Runtime';
  static const String colPressure = 'pressure';
  static const String colComment = 'comment';
  static const String colIsSynchronized = 'isSynchronized';

  Database? _database;

  MeasurementProvider();

  Future<Database> get database async {
    // databaseFactory = databaseFactoryFfi;
    if (_database != null && _database!.isOpen) return _database!;
    _database = await initializeDatabase();
    return _database!;
  }

  Future<Database> initializeDatabase() async {
    final String path = await getDatabasesPath();
    final String dbPath = join(path, 'measurement.db');

    final db = await openDatabase(
      dbPath,
      version: 1,
      onCreate: (db, version) async {
        await db.execute('''
          CREATE TABLE $mainTable (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            $colDate TEXT NOT NULL,
            $colStationName TEXT NOT NULL,
            $colTime TEXT,
            $colPowerMeter INTEGER NOT NULL,
            $colP1Runtime REAL,
            $colP2Runtime REAL,
            $colPressure REAL,
            $colComment TEXT,
            $colIsSynchronized INTEGER NOT NULL,
            UNIQUE ($colStationName, $colDate) ON CONFLICT REPLACE
          )
        ''');
        await db.execute('''
          CREATE TABLE $tblDaily (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            $colDate TEXT NOT NULL,
            $colStationName TEXT NOT NULL,
            $colTime TEXT,
            $colPowerMeter REAL NOT NULL,
            $colP1Runtime REAL,
            $colP2Runtime REAL,
            $colPressure REAL,
            $colComment TEXT,
            $colIsSynchronized INTEGER NOT NULL,
            UNIQUE ($colStationName, $colDate) ON CONFLICT REPLACE
          )
        ''');
      },
    );
    return db;
  }

  bool compareMeasurements(
      Measurement existingMeasurement, Measurement newMeasurement) {
    final modifiedMeasurement = newMeasurement.copyWith(
      id: existingMeasurement.id,
      time: existingMeasurement.time,
    );

    bool result = true;

    if (modifiedMeasurement.id != existingMeasurement.id ||
        modifiedMeasurement.time != existingMeasurement.time) {
      result = false;
    }

    if (modifiedMeasurement.powerMeter != existingMeasurement.powerMeter ||
        modifiedMeasurement.isSynchronized !=
            existingMeasurement.isSynchronized ||
        modifiedMeasurement.p1Runtime != existingMeasurement.p1Runtime ||
        modifiedMeasurement.p2Runtime != existingMeasurement.p2Runtime ||
        modifiedMeasurement.pressure != existingMeasurement.pressure ||
        modifiedMeasurement.comment != existingMeasurement.comment) {
      result = false;
    }

    return result;
  }

  Future<int> createMeasurement(
      {required String table, required Measurement measurement}) async {
    try {
      final db = await database;
      final id = await db.insert(table, measurement.toMap(),
          conflictAlgorithm: ConflictAlgorithm.replace);
      return id;
    } catch (e) {
      if (kDebugMode) {
        print('Error creating measurement: $e');
      }
      return -1;
    }
  }

  Future<Measurement?> getMeasurement(
      String table, String stationName, DateTime date) async {
    try {
      final db = await database;
      final measurements = await db.query(table,
          where: '$colStationName = ? AND $colDate = ?',
          whereArgs: [stationName, date.toIso8601String().substring(0, 10)]);

      if (measurements.isNotEmpty) {
        return Measurement.fromMap(measurements.first).cleanDecimals();
      }
      return null;
    } catch (e) {
      // Log the error and return null or handle the error as needed
      if (kDebugMode) {
        print('Error fetching measurement: $e');
      }
      return null; // Indicate an error by returning null
    }
  }

  Future<Map<String, Measurement?>?> getCurrentAndAdjacentMeasurementsForDate({
    required String table,
    required String stationName,
    required DateTime date,
  }) async {
    try {
      final db = await database;

      final previousDateMeasurement = await db.query(
        table,
        where: '$colStationName = ? AND $colDate < ?',
        whereArgs: [stationName, date.toIso8601String().substring(0, 10)],
        orderBy: '$colDate DESC',
        limit: 1,
      );
      if (kDebugMode) {
        print('got previous measurement.. $previousDateMeasurement.');
      }

      final currentDateMeasurement = await db.query(
        table,
        where: '$colStationName = ? AND $colDate = ?',
        whereArgs: [stationName, date.toIso8601String().substring(0, 10)],
      );

      if (kDebugMode) {
        print('got current measurement...$currentDateMeasurement');
      }

      final futureDateMeasurement = await db.query(
        table,
        where: '$colStationName = ? AND $colDate > ?',
        whereArgs: [stationName, date.toIso8601String().substring(0, 10)],
        orderBy: '$colDate ASC',
        limit: 1,
      );

      if (kDebugMode) {
        print('got future measurement...$futureDateMeasurement');
      }

      final Map<String, Measurement> measurements = {};

      if (previousDateMeasurement.isNotEmpty) {
        measurements['previous'] =
            (Measurement.fromMap(previousDateMeasurement.first));
      }

      if (currentDateMeasurement.isNotEmpty) {
        measurements['current'] =
            (Measurement.fromMap(currentDateMeasurement.first));
      }

      if (futureDateMeasurement.isNotEmpty) {
        measurements['next'] =
            (Measurement.fromMap(futureDateMeasurement.first));
      }

      if (kDebugMode) {
        print('measurements: $measurements');
      }
      return measurements;
    } catch (e) {
      if (kDebugMode) {
        print('Error fetching measurements for date range: $e');
      }
      rethrow;
    }
  }

  // ? Update in the mainTable only
  Future<int> updateMeasurement(String table, Measurement measurement) async {
    try {
      final db = await database;
      return await db.update(
        table,
        measurement.toMap(),
        where: 'id = ?',
        whereArgs: [measurement.id],
      );
    } catch (e) {
      if (kDebugMode) {
        print('Error updating measurement: $e');
      }
      return -1; // Indicate an error
    }
  }

  Future<List<Measurement>?> getMeasurementsForStationInDateRange(String table,
      String stationName, DateTime startDate, DateTime endDate) async {
    try {
      final db = await database;
      final measurements = await db.query(
        table,
        where: '$colStationName = ? AND $colDate >= ? AND $colDate <= ?',
        orderBy: '$colDate ASC',
        whereArgs: [
          stationName,
          startDate.toIso8601String().substring(0, 10),
          endDate.toIso8601String().substring(0, 10),
        ],
      );

      if (measurements.isNotEmpty) {
        return measurements.map((map) => Measurement.fromMap(map)).toList();
      } else {
        return null;
      }
    } catch (e) {
      // Handle the error, e.g., log it or throw an exception
      if (kDebugMode) {
        print('Error getting measurements: $e');
      }
      rethrow; // Throw an exception to indicate an error
    }
  }

  Future<void> dropTables() async {
    try {
      final db = await database;
      await db.execute('DROP TABLE IF EXISTS $mainTable');
      await db.execute('DROP TABLE IF EXISTS $tblDaily');
    } catch (e) {
      // Handle the error, e.g., log it or throw it again if needed
      if (kDebugMode) {
        print('Error dropping tables: $e');
      }
    }
  }
}
