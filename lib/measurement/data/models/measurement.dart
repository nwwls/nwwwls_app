// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:flutter/material.dart';

class Measurement {
  int? id;
  DateTime date;
  String stationName;
  TimeOfDay? time;
  num powerMeter;
  num? p1Runtime;
  num? p2Runtime;
  num? pressure;
  String? comment;
  bool isSynchronized;
  Measurement({
    this.id,
    required this.date,
    required this.stationName,
    this.time,
    required this.powerMeter,
    this.p1Runtime,
    this.p2Runtime,
    this.pressure,
    this.comment,
    this.isSynchronized = false,
  });

  factory Measurement.empty() {
    return Measurement(
      date: DateTime.now(),
      time: TimeOfDay.now(),
      stationName: '',
      powerMeter: 0,
      isSynchronized: false,
    );
  }

  @override
  String toString() {
    return 'Measurement(id: $id, date: $date, stationName: $stationName, time: $time, powerMeter: $powerMeter, p1Runtime: $p1Runtime, p2Runtime: $p2Runtime, pressure: $pressure, comment: $comment, isSynchronized: $isSynchronized)';
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'date': date.toIso8601String().substring(0, 10),
      'stationName': stationName,
      'time': _serializeTimeOfDay(time),
      'powerMeter': powerMeter.toInt(),
      'p1Runtime': p1Runtime,
      'p2Runtime': p2Runtime,
      'pressure': pressure,
      'comment': comment,
      'isSynchronized': isSynchronized ? 1 : 0,
    };
  }

  factory Measurement.fromMap(Map<String, dynamic> map) {
    final measurement = Measurement(
      id: map['id'] != null ? map['id'] as int : null,
      date: DateTime.parse(map['date']),
      stationName: map['stationName'] as String,
      time: _deserializeTimeOfDay(map['time']),
      powerMeter: (map['powerMeter'] as num).toInt(),
      p1Runtime: map['p1Runtime'] != null ? map['p1Runtime'] as num : null,
      p2Runtime: map['p2Runtime'] != null ? map['p2Runtime'] as num : null,
      pressure: map['pressure'] != null ? map['pressure'] as num : null,
      comment: map['comment'] != null ? map['comment'] as String : null,
      isSynchronized: (map['isSynchronized'] as int) == 1,
    );
    return measurement;
  }
  String _serializeTimeOfDay(TimeOfDay? time) {
    if (time != null) {
      final minutes = time.hour * 60 + time.minute;
      return minutes.toString();
    }
    return '';
  }

  static TimeOfDay? _deserializeTimeOfDay(String? timeString) {
    if (timeString != null && timeString.isNotEmpty) {
      final minutes = int.tryParse(timeString);
      if (minutes != null) {
        final hours = minutes ~/ 60;
        final remainingMinutes = minutes % 60;
        return TimeOfDay(hour: hours, minute: remainingMinutes);
      }
    }
    return null;
  }

  String toJson() => json.encode(toMap());

  factory Measurement.fromJson(String source) =>
      Measurement.fromMap(json.decode(source) as Map<String, dynamic>);

  Measurement copyWith({
    int? id,
    DateTime? date,
    String? stationName,
    TimeOfDay? time,
    num? powerMeter,
    num? p1Runtime,
    num? p2Runtime,
    num? pressure,
    String? comment,
    bool? isSynchronized,
  }) {
    return Measurement(
      id: id ?? this.id,
      date: date ?? this.date,
      stationName: stationName ?? this.stationName,
      time: time ?? this.time,
      powerMeter: powerMeter?.toDouble() ?? this.powerMeter,
      p1Runtime: p1Runtime ?? this.p1Runtime,
      p2Runtime: p2Runtime ?? this.p2Runtime,
      pressure: pressure ?? this.pressure,
      comment: comment ?? this.comment,
      isSynchronized: isSynchronized ?? this.isSynchronized,
    );
  }

  Duration calculateTimeDifference(TimeOfDay? time1, TimeOfDay? time2) {
    if (time1 == null && time2 == null) {
      return Duration.zero;
    } else if (time1 == null || time2 == null) {
      final diff = time1 ?? time2;
      return Duration(minutes: diff!.hour * 60 + diff.minute);
    } else {
      final time1Minutes = time1.hour * 60 + time1.minute;
      final time2Minutes = time2.hour * 60 + time2.minute;
      return Duration(minutes: (time1Minutes - time2Minutes).abs());
    }
  }

  Measurement createNormalizedMeasurement(Measurement oldMeasurement) {
    final timeDifference = calculateTimeDifference(time, oldMeasurement.time);
    final dayDifference = date.difference(oldMeasurement.date).inDays;

    final factor = (timeDifference.inSeconds + dayDifference * 86400) /
        86400; // Normalize to days

    // Calculate normalized values
    final normalizedPowerMeter =
        (powerMeter - oldMeasurement.powerMeter).abs() / factor;

    final normalizedP1Runtime =
        (p1Runtime != null && oldMeasurement.p1Runtime != null)
            ? (p1Runtime! - oldMeasurement.p1Runtime!) / factor
            : null;
    final normalizedP2Runtime =
        (p2Runtime != null && oldMeasurement.p2Runtime != null)
            ? (p2Runtime! - oldMeasurement.p2Runtime!) / factor
            : null;

    // Create a new normalized Measurement object using copyWith
    final normalizedMeasurement = copyWith(
      powerMeter: normalizedPowerMeter,
      p1Runtime: normalizedP1Runtime,
      p2Runtime: normalizedP2Runtime,
      // Update other properties as needed
    );

    return normalizedMeasurement;
  }

  Measurement cleanDecimals() {
    return Measurement(
      id: id,
      date: date,
      stationName: stationName,
      time: time,
      powerMeter:
          powerMeter.floor() == powerMeter ? powerMeter.toInt() : powerMeter,
      p1Runtime:
          p1Runtime?.floor() == p1Runtime ? p1Runtime?.toInt() : p1Runtime,
      p2Runtime:
          p2Runtime?.floor() == p2Runtime ? p2Runtime?.toInt() : p2Runtime,
      pressure: pressure?.floor() == pressure ? pressure?.toInt() : pressure,
      comment: comment,
      isSynchronized: isSynchronized,
    );
  }

  @override
  bool operator ==(covariant Measurement other) {
    if (identical(this, other)) return true;

    return other.id == id &&
        other.date == date &&
        other.stationName == stationName &&
        other.time == time &&
        other.powerMeter == powerMeter &&
        other.p1Runtime == p1Runtime &&
        other.p2Runtime == p2Runtime &&
        other.pressure == pressure &&
        other.comment == comment &&
        other.isSynchronized == isSynchronized;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        date.hashCode ^
        stationName.hashCode ^
        time.hashCode ^
        powerMeter.hashCode ^
        p1Runtime.hashCode ^
        p2Runtime.hashCode ^
        pressure.hashCode ^
        comment.hashCode ^
        isSynchronized.hashCode;
  }
}
