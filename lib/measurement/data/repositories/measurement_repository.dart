import 'package:flutter/foundation.dart';
import '../models/measurement.dart';
import '../providers/measurement_provider.dart';

class MeasurementRepository {
  final MeasurementProvider _provider;

  MeasurementRepository(this._provider);

  Future<Measurement?> getMeasurement(
      String table, String stationName, DateTime date) async {
    try {
      return await _provider.getMeasurement(table, stationName, date);
    } catch (e) {
      if (kDebugMode) {
        print('Repository error in getMeasurement: $e');
      }
      return null; // Indicate an error
    }
  }

  Future<int> updateMeasurement(String table, Measurement measurement) async {
    try {
      return await _provider.updateMeasurement(table, measurement);
    } catch (e) {
      if (kDebugMode) {
        print('Repository error in updateMeasurement: $e');
      }
      return -1; // Indicate an error
    }
  }

  Future<List<Measurement>?> getMeasurementsForStationInDateRange(String table,
      String stationName, DateTime startDate, DateTime endDate) async {
    try {
      return await _provider.getMeasurementsForStationInDateRange(
          table, stationName, startDate, endDate);
    } catch (e) {
      if (kDebugMode) {
        print('Repository error in getMeasurementsForStationInDateRange: $e');
      }
      return null; // Return an empty list to indicate an error
    }
  }

  Future<void> dropTables() async {
    try {
      await _provider.dropTables();
    } catch (e) {
      if (kDebugMode) {
        print('Repository error in dropTables: $e');
      }
    }
  }

  Future<int> createOrUpdateMeasurement(Measurement measurement) async {
    try {
      if (kDebugMode) {
        print('Receiving Measurements...');
      }
      final recievedMeasurements =
          await _provider.getCurrentAndAdjacentMeasurementsForDate(
        table: MeasurementProvider.mainTable,
        stationName: measurement.stationName,
        date: measurement.date,
      );
      if (kDebugMode) {
        print('Received Measurements: $recievedMeasurements');
      }

      // ? No measurements exist => first for the station
      if (recievedMeasurements!.isEmpty) {
        if (kDebugMode) {
          print('No measurements exist for this station');
          print(
              'Creating measurement in ${MeasurementProvider.mainTable} for $measurement...');
        }
        // insert the measurement in measurements table
        final id = await _provider.createMeasurement(
            table: MeasurementProvider.mainTable, measurement: measurement);
        if (kDebugMode) {
          print('Measurement created: $id');
        }
        return id;
      } else {
        if (kDebugMode) {
          print('Some measurements already exist.');
        }

        // ? creating new measurement
        if (recievedMeasurements['current'] == null) {
          if (kDebugMode) {
            print('Current measurement is missing.');
          }

          // ? nothing exists before but something exists after => creating new initial measurement and a new initial normalised for the old initial measurement
          if (recievedMeasurements['previous'] == null &&
              recievedMeasurements['next'] != null) {
            if (kDebugMode) {
              print('only next measurement exist');
            }
            // create a new normalised measurement
            final normalisedMeasurement = recievedMeasurements['next']!
                .createNormalizedMeasurement(measurement);
            // insert new normalised measurement in tblDaily
            final newNormalisedMeasurementId =
                await _provider.createMeasurement(
                    table: MeasurementProvider.tblDaily,
                    measurement: normalisedMeasurement);
            // insert the measurement in measurements table
            final id = await _provider.createMeasurement(
                table: MeasurementProvider.mainTable, measurement: measurement);
            return id;
          } else if (recievedMeasurements['previous'] != null &&
              recievedMeasurements['next'] != null) {
            if (kDebugMode) {
              print('both prior and next measurement exist');
            }
            //  create a new normalised measurement
            final newNormalisedMeasurement = measurement
                .createNormalizedMeasurement(recievedMeasurements['previous']!);

            //  insert the new normalised measurement
            final newNormalisedMeasurementId =
                await _provider.createMeasurement(
                    table: MeasurementProvider.tblDaily,
                    measurement: newNormalisedMeasurement);

            // get the existing/next normalised measurement
            final oldNormalisedMeasurement = await _provider.getMeasurement(
                MeasurementProvider.tblDaily,
                measurement.stationName,
                recievedMeasurements['next']!.date);

            // create an updated version of the old normalised measurement
            Measurement updatedNormalisedMeasurement =
                recievedMeasurements['next']!
                    .createNormalizedMeasurement(measurement);
            updatedNormalisedMeasurement = updatedNormalisedMeasurement
                .copyWith(id: oldNormalisedMeasurement!.id);

            // update the existing normalised measurement
            final updatedNormalisedMeasurementId =
                await _provider.updateMeasurement(
                    MeasurementProvider.tblDaily, updatedNormalisedMeasurement);

            // insert the measurement into the measurement table
            final id = await _provider.createMeasurement(
                table: MeasurementProvider.mainTable, measurement: measurement);
            return id;
          } else {
            if (kDebugMode) {
              print('Only prior measurement exist');
            }
            // create a new normalised measurement
            final newNormalisedMeasurement = measurement
                .createNormalizedMeasurement(recievedMeasurements['previous']!);

            // insert the new normalised measurement in the tblDaily
            final newNormalisedMeasurementId =
                await _provider.createMeasurement(
                    table: MeasurementProvider.tblDaily,
                    measurement: newNormalisedMeasurement);
            // insert the new measurement in the measuerments table
            final id = _provider.createMeasurement(
                table: MeasurementProvider.mainTable, measurement: measurement);
            return id;
          }
        } else {
          if (kDebugMode) {
            print('Current measurement exists.');
          }

          if (recievedMeasurements['previous'] == null &&
              recievedMeasurements['next'] != null) {
            if (kDebugMode) {
              print('only next measurement exists');
            }
            // get the existing/next normalised measurement
            final oldNormalisedMeasurement = await _provider.getMeasurement(
                MeasurementProvider.tblDaily,
                measurement.stationName,
                recievedMeasurements['next']!.date);

            // create an updated version of the old normalised measurement
            Measurement updatedNormalisedMeasurement =
                recievedMeasurements['next']!
                    .createNormalizedMeasurement(measurement);
            updatedNormalisedMeasurement = updatedNormalisedMeasurement
                .copyWith(id: oldNormalisedMeasurement!.id);

            // update the existing normalised measurement
            final updatedNormalisedMeasurementId =
                await _provider.updateMeasurement(
                    MeasurementProvider.tblDaily, updatedNormalisedMeasurement);

            // update the existing measurement
            measurement =
                measurement.copyWith(id: recievedMeasurements['current']!.id);
            final id = await _provider.updateMeasurement(
                MeasurementProvider.mainTable, measurement);
            return id;
          } else if (recievedMeasurements['previous'] != null &&
              recievedMeasurements['next'] != null) {
            if (kDebugMode) {
              print('both prior and next measurements exist');
            }
            // get the current normalised measurement
            final currentNormalisedMeasurement = await _provider.getMeasurement(
                MeasurementProvider.tblDaily,
                measurement.stationName,
                measurement.date);

            //  create an updated version of the current normalised measurement
            Measurement updatedCurrentNormalisedMeasurement = measurement
                .createNormalizedMeasurement(recievedMeasurements['previous']!);
            updatedCurrentNormalisedMeasurement =
                updatedCurrentNormalisedMeasurement.copyWith(
                    id: currentNormalisedMeasurement!.id);

            // update the new current normalised measurement
            final updatedCurrentNormalisedMeasurementId =
                await _provider.updateMeasurement(MeasurementProvider.tblDaily,
                    updatedCurrentNormalisedMeasurement);

            // get the next normalised measurement
            final oldNormalisedMeasurement = await _provider.getMeasurement(
                MeasurementProvider.tblDaily,
                measurement.stationName,
                recievedMeasurements['next']!.date);

            // create an updated version of the old normalised measurement
            Measurement updatedNormalisedMeasurement =
                recievedMeasurements['next']!
                    .createNormalizedMeasurement(measurement);
            updatedNormalisedMeasurement = updatedNormalisedMeasurement
                .copyWith(id: oldNormalisedMeasurement!.id);

            // update the next normalised measurement
            final updatedNormalisedMeasurementId =
                await _provider.updateMeasurement(
                    MeasurementProvider.tblDaily, updatedNormalisedMeasurement);

            // update the existing measurement
            measurement =
                measurement.copyWith(id: recievedMeasurements['current']!.id);
            final id = await _provider.updateMeasurement(
                MeasurementProvider.mainTable, measurement);
            return id;
          } else if (recievedMeasurements['previous'] != null &&
              recievedMeasurements['next'] == null) {
            if (kDebugMode) {
              print('only prior measurement exists');
            }
            // get the current normalised measurement
            final currentNormalisedMeasurement = await _provider.getMeasurement(
                MeasurementProvider.tblDaily,
                measurement.stationName,
                measurement.date);

            //  create an updated version of the current normalised measurement
            Measurement updatedCurrentNormalisedMeasurement = measurement
                .createNormalizedMeasurement(recievedMeasurements['previous']!);
            updatedCurrentNormalisedMeasurement =
                updatedCurrentNormalisedMeasurement.copyWith(
                    id: currentNormalisedMeasurement!.id);

            // update the new current normalised measurement
            final currentUpdatedCurrentNormalisedMeasurementId =
                await _provider.updateMeasurement(MeasurementProvider.tblDaily,
                    updatedCurrentNormalisedMeasurement);

            // update the existing measurement

            final id = await _provider.updateMeasurement(
                MeasurementProvider.mainTable,
                measurement.copyWith(id: recievedMeasurements['current']!.id));

            return id;
          } else {
            // update the existing measurement
            measurement =
                measurement.copyWith(id: recievedMeasurements['current']!.id);
            final id = await _provider.updateMeasurement(
                MeasurementProvider.mainTable, measurement);
            return id;
          }
        }
      }
    } catch (e) {
      if (kDebugMode) {
        print('Error creating or updating measurement: $e');
      }
      return -1;
    }
  }
}
