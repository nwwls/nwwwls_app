// ignore_for_file: invalid_use_of_visible_for_testing_member

import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:nwwwls_app/measurement/data/providers/measurement_provider.dart';

import '../../data/models/measurement.dart';
import '../../data/repositories/measurement_repository.dart';

part 'measurement_event.dart';
part 'measurement_state.dart';

class MeasurementBloc extends Bloc<MeasurementEvent, MeasurementState> {
  final MeasurementRepository _repository;
  MeasurementBloc(this._repository) : super(MeasurementInitial()) {
    on<CreateOrUpdateMeasurementEvent>(_createMeasurement);
    on<FetchMeasurementEvent>(_fetchMeasurement);
    on<FetchMeasurementsForStationInDateRangeEvent>(
        _fetchMeasurementsInDateRange);
  }
  FutureOr<void> _createMeasurement(CreateOrUpdateMeasurementEvent event,
      Emitter<MeasurementState> state) async {
    emit(MeasurementCreateOrUpdateInProgress());

    try {
      final id = await _repository.createOrUpdateMeasurement(event.measurement);
      if (id != -1) {
        emit(MeasurementCreateOrUpdateSuccess(id));
        if (kDebugMode) {
          print('measurement bloc has emitted sucess state ...');
        }
      } else {
        emit(MeasurementError('Could not save measurement'));
      }
    } catch (err) {
      _handleError(err, state, 'Failed to create measurement');
    }
  }

  Future<void> _fetchMeasurement(
      FetchMeasurementEvent event, Emitter<MeasurementState> state) async {
    _emitLoadingState(state);
    try {
      final measurement = await _repository.getMeasurement(
          MeasurementProvider.mainTable, event.stationName, event.date);
      if (measurement != null) {
        emit(MeasurementLoaded(measurement));
      } else {
        emit(MeasurementEmpty());
      }
    } catch (err) {
      _handleError(err, state, 'Failed to load measurement');
    }
  }

  Future<void> _fetchMeasurementsInDateRange(
      FetchMeasurementsForStationInDateRangeEvent event,
      Emitter<MeasurementState> state) async {
    _emitLoadingState(state);
    try {
      final measurements =
          await _repository.getMeasurementsForStationInDateRange(
        MeasurementProvider.tblDaily,
        event.stationName,
        event.startDate,
        event.endDate,
      );
      if (measurements != null) {
        emit(MeasurementsLoaded(measurements));
      } else {
        emit(MeasurementEmpty());
      }
    } catch (err) {
      _handleError(err, state, 'Failed to load measurement(s)');
    }
  }

  void _handleError(
      dynamic error, Emitter<MeasurementState> state, String message) {
    state(MeasurementError('$message: $error'));
  }

  void _emitLoadingState(Emitter<MeasurementState> state) {
    state(MeasurementLoading());
  }
}
