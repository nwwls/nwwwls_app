part of 'measurement_bloc.dart';

sealed class MeasurementEvent extends Equatable {
  const MeasurementEvent();

  @override
  List<Object> get props => [];
}

class CreateOrUpdateMeasurementEvent extends MeasurementEvent {
  final Measurement measurement;

  const CreateOrUpdateMeasurementEvent(this.measurement);
}

class FetchMeasurementEvent extends MeasurementEvent {
  final String stationName;
  final DateTime date;

  const FetchMeasurementEvent({required this.stationName, required this.date});
}

class FetchMeasurementsForStationInDateRangeEvent extends MeasurementEvent {
  final String stationName;
  final DateTime startDate;
  final DateTime endDate;

  const FetchMeasurementsForStationInDateRangeEvent(
      this.stationName, this.startDate, this.endDate);
}
