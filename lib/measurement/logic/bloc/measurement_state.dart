part of 'measurement_bloc.dart';

abstract class MeasurementState extends Equatable {}

class MeasurementInitial extends MeasurementState {
  @override
  List<Object?> get props => [];
}

class MeasurementLoading extends MeasurementState {
  @override
  List<Object?> get props => [];
}

class MeasurementEmpty extends MeasurementState {
  @override
  List<Object?> get props => [];
}

class MeasurementsLoaded extends MeasurementState {
  final List<Measurement> measurements;

  MeasurementsLoaded(this.measurements);

  @override
  List<Object?> get props => [measurements];
}

class MeasurementLoaded extends MeasurementState {
  final Measurement measurement;

  MeasurementLoaded(this.measurement);

  @override
  List<Object?> get props => [measurement];
}

class MeasurementCreateOrUpdateInProgress extends MeasurementState {
  @override
  List<Object?> get props => [];
}

class MeasurementCreateOrUpdateSuccess extends MeasurementState {
  final int measurementId;

  MeasurementCreateOrUpdateSuccess(this.measurementId);

  @override
  List<Object?> get props => [measurementId];
}

class MeasurementError extends MeasurementState {
  final String errorMessage;

  MeasurementError(this.errorMessage);

  @override
  List<Object?> get props => [errorMessage];
}
