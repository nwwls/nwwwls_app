import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/logic/cubit/theme_cubit.dart';
import '../../../measurement/logic/bloc/measurement_bloc.dart';
import '../../../measurement/presentation/screens/new_data_screen.dart';
import '../../../measurement/presentation/screens/view_data_form_screen.dart';
import '../../../core/screens/settings_screen.dart';

class LoggedInScreen extends StatefulWidget {
  const LoggedInScreen({super.key});

  @override
  State<LoggedInScreen> createState() => _LoggedInScreenState();
}

class _LoggedInScreenState extends State<LoggedInScreen> {
  int currentPageIndex = 0;

  final List<Widget> _screens = [
    const NewDataScreen(),
    const ViewDataFormScreen(),
    const SettingsScreen(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: NavigationBar(
        destinations: const [
          NavigationDestination(
            icon: Icon(Icons.note_add_outlined),
            selectedIcon: Icon(Icons.note_add),
            label: 'New Data',
          ),
          NavigationDestination(
            icon: Icon(Icons.leaderboard_outlined),
            selectedIcon: Icon(Icons.leaderboard),
            label: 'View Data',
          ),
          NavigationDestination(
            icon: Icon(Icons.settings_outlined),
            selectedIcon: Icon(Icons.settings),
            label: 'Settings',
          ),
        ],
        labelBehavior: NavigationDestinationLabelBehavior.onlyShowSelected,
        animationDuration: const Duration(milliseconds: 300),
        height: 72,
        indicatorColor: Theme.of(context).colorScheme.secondaryContainer,
        selectedIndex: currentPageIndex,
        onDestinationSelected: (index) {
          setState(() {
            currentPageIndex = index;
          });
        },
      ),
      body: BlocProvider.value(
        value: BlocProvider.of<ThemeCubit>(context),
        child: BlocProvider.value(
          value: BlocProvider.of<MeasurementBloc>(context),
          child: SafeArea(
            child: IndexedStack(
              index: currentPageIndex,
              children: _screens,
            ),
          ),
        ),
      ),
    );
  }
}
