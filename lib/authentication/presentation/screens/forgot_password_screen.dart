import 'package:flutter/material.dart';

import '../widgets/reset_password_form.dart';

class ForgotPasswordScreen extends StatelessWidget {
  const ForgotPasswordScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Forgot password?'),
      ),
      body: const SingleChildScrollView(
        reverse: true,
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: ResetPasswordForm(),
        ),
      ),
    );
  }
}
