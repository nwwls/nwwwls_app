import 'package:flutter/material.dart';

import '../../../core/widgets/input_fields.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        EmailField(controller: TextEditingController()),
        const SizedBox(height: 12.0),
        const PasswordField(labelText: 'Password'),
        const SizedBox(height: 4.0),
        Container(
          alignment: Alignment.centerRight,
          child: TextButton(
            onPressed: () =>
                Navigator.of(context).pushNamed('/forgot-password'),
            child: const Text('Forgot password?'),
          ),
        ),
        const SizedBox(height: 12.0),
        FilledButton.icon(
          onPressed: () =>
              Navigator.of(context).pushReplacementNamed('/logged-in'),
          icon: const Icon(Icons.engineering_outlined),
          label: const Text('Login'),
        )
      ],
    );
  }
}
// TODO: Implement Validators via bloc or TextInputFormatters