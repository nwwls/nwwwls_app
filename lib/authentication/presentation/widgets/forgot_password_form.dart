import 'package:flutter/material.dart';

import '../../../core/widgets/input_fields.dart';

class ForgotPasswordForm extends StatelessWidget {
  const ForgotPasswordForm({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        EmailField(controller: TextEditingController()),
        const SizedBox(height: 12.0),
        FilledButton.icon(
          onPressed: () => Navigator.of(context).pushNamed('/reset-password'),
          icon: const Icon(Icons.outgoing_mail),
          label: const Text('Send password reset email'),
        ),
      ],
    );
  }
}
