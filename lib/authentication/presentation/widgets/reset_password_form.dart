import 'package:flutter/material.dart';

import '../../../core/widgets/input_fields.dart';

class ResetPasswordForm extends StatelessWidget {
  const ResetPasswordForm({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        const TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Verification Code',
            prefixIcon: Icon(Icons.pin_outlined),
          ),
        ),
        const SizedBox(height: 12.0),
        const PasswordField(
          labelText: 'New Password',
        ),
        const SizedBox(height: 12.0),
        const PasswordField(labelText: 'Confirm New Password'),
        const SizedBox(height: 12.0),
        FilledButton.icon(
          onPressed: () {
            Navigator.of(context).pushNamed('/data-entry');
          },
          icon: const Icon(Icons.lock_reset_outlined),
          label: const Text('RESET PASSWORD'),
        )
      ],
    );
  }
}
