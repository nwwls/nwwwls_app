import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:nwwwls_app/measurement/data/models/measurement.dart';
import 'package:nwwwls_app/measurement/data/repositories/measurement_repository.dart';
import 'package:nwwwls_app/measurement/logic/bloc/measurement_bloc.dart';

class MockMeasurementRepository extends Mock implements MeasurementRepository {}

void main() {
  group('Measurement bloc tests - success states', () {
    late MeasurementBloc bloc;
    late MockMeasurementRepository mockRepository;
    late Measurement measurement;
    late DateTime date;
    late String stationName;
    late DateTime startDate;
    late DateTime endDate;
    registerFallbackValue(Measurement.empty());

    setUp(() {
      mockRepository = MockMeasurementRepository();
      bloc = MeasurementBloc(mockRepository);
      measurement = Measurement(
        stationName: 'Station A',
        date: DateTime.now(),
        powerMeter: 123,
      );
      stationName = 'Station A';
      date = DateTime.now();
      startDate = DateTime(2023, 1, 1);
      endDate = DateTime(2023, 12, 31);
    });
    tearDown(() {
      bloc.close();
    });

    test(
        'emits MeasurementCreateOrUpdateSuccess when CreateOrUpdateMeasurementEvent is added',
        () async {
      // ? Arrange: Prepare the event you want to test.

      final createEvent = CreateOrUpdateMeasurementEvent(measurement);

      // Mock the repository's behavior when createOrUpdateMeasurement is called.
      when(() => mockRepository.createOrUpdateMeasurement(any()))
          .thenAnswer((_) async => 42);

      // ? Act: Dispatch the event to the bloc.
      bloc.add(createEvent);

      // ? Assert: Ensure that the expected states are emitted.
      await expectLater(
        bloc.stream,
        emitsInOrder([
          MeasurementCreateOrUpdateInProgress(),
          MeasurementCreateOrUpdateSuccess(42),
        ]),
      );
    });
    test('emits MeasurementLoaded when FetchMeasurementEvent is added',
        () async {
      // Arrange: Prepare the event you want to test.

      final fetchEvent =
          FetchMeasurementEvent(stationName: stationName, date: date);

      // Mock the repository's behavior when getMeasurement is called.

      when(() => mockRepository.getMeasurement(any(), any(), any()))
          .thenAnswer((_) async => measurement);

      // Act: Dispatch the event to the bloc.
      bloc.add(fetchEvent);

      // Assert: Ensure that the expected states are emitted.
      await expectLater(
        bloc.stream,
        emitsInOrder([
          MeasurementLoading(),
          MeasurementsLoaded([measurement]),
        ]),
      );
    });
    test(
        'emits MeasurementLoaded when FetchMeasurementsForStationInDateRangeEvent is added',
        () async {
      // Arrange: Prepare the event you want to test.

      final fetchInDateRange = FetchMeasurementsForStationInDateRangeEvent(
          stationName, startDate, endDate);

      // Mock the repository's behavior when getMeasurementsForStationInDateRange is called.
      final measurements = [measurement];
      when(() => mockRepository.getMeasurementsForStationInDateRange(
          any(), any(), any(), any())).thenAnswer((_) async => measurements);

      // Act: Dispatch the event to the bloc.
      bloc.add(fetchInDateRange);

      // Assert: Ensure that the expected states are emitted.
      await expectLater(
        bloc.stream,
        emitsInOrder([
          MeasurementLoading(),
          MeasurementsLoaded(measurements),
        ]),
      );
    });
  });

  group('Measurement bloc tests - error states', () {
    late MeasurementBloc bloc;
    late MockMeasurementRepository mockRepository;
    late Measurement measurement;
    late DateTime date;
    late String stationName;
    late DateTime startDate;
    late DateTime endDate;
    registerFallbackValue(Measurement.empty());

    setUp(() {
      mockRepository = MockMeasurementRepository();
      bloc = MeasurementBloc(mockRepository);
      measurement = Measurement(
        stationName: 'Station A',
        date: DateTime.now(),
        powerMeter: 123,
      );
      stationName = 'Station A';
      date = DateTime.now();
      startDate = DateTime(2023, 1, 1);
      endDate = DateTime(2023, 12, 31);
    });
    tearDown(() {
      bloc.close();
    });

    test(
        'emits MeasurementCreateOrUpdateFailure on error in CreateOrUpdateMeasurementEvent',
        () async {
      // Arrange: Prepare the event you want to test.

      final event = CreateOrUpdateMeasurementEvent(measurement);

      // Mock the repository's behavior to throw an error.
      when(() => mockRepository.createOrUpdateMeasurement(any()))
          .thenThrow('Some error message');

      // Act: Dispatch the event to the bloc.
      bloc.add(event);

      // Assert: Ensure that the expected error state is emitted.
      await expectLater(
        bloc.stream,
        emitsInOrder([
          MeasurementCreateOrUpdateInProgress(),
          MeasurementError('Failed to create measurement: Some error message'),
        ]),
      );
    });
    test('emits MeasurementError on error in FetchMeasurementEvent', () async {
      // Arrange: Prepare the event you want to test.

      final event = FetchMeasurementEvent(stationName: stationName, date: date);

      // Mock the repository's behavior to throw an error.
      when(() => mockRepository.getMeasurement(any(), any(), any()))
          .thenThrow('Some error message');

      // Act: Dispatch the event to the bloc.
      bloc.add(event);

      // Assert: Ensure that the expected error state is emitted.
      await expectLater(
        bloc.stream,
        emitsInOrder([
          MeasurementLoading(),
          MeasurementError('Failed to load measurement: Some error message'),
        ]),
      );
    });
    test(
        'emits MeasurementError on error in FetchMeasurementsForStationInDateRangeEvent',
        () async {
      // Arrange: Prepare the event you want to test.

      final event = FetchMeasurementsForStationInDateRangeEvent(
          stationName, startDate, endDate);

      // Mock the repository's behavior to throw an error.
      when(() => mockRepository.getMeasurementsForStationInDateRange(
          any(), any(), any(), any())).thenThrow('Some error message');

      // Act: Dispatch the event to the bloc.
      bloc.add(event);

      // Assert: Ensure that the expected error state is emitted.
      await expectLater(
        bloc.stream,
        emitsInOrder([
          MeasurementLoading(),
          MeasurementError('Failed to load measurement(s): Some error message'),
        ]),
      );
    });
  });
}
