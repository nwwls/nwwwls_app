import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:nwwwls_app/measurement/data/models/measurement.dart';
import 'package:nwwwls_app/measurement/data/providers/measurement_provider.dart';
import 'package:nwwwls_app/measurement/data/repositories/measurement_repository.dart';

class MockMeasurementProvider extends Mock implements MeasurementProvider {}

class MockMeasurement extends Mock implements Measurement {}

void main() {
  late MeasurementProvider mockProvider;
  late MeasurementRepository repository;

  setUp(() {
    mockProvider = MockMeasurementProvider();
    repository = MeasurementRepository(mockProvider);
    registerFallbackValue(Measurement.empty());

    // Mock bechaviour for provider's create method
    when(() => mockProvider.createMeasurement(
          table: any<String>(named: 'table'),
          measurement: any<Measurement>(named: 'measurement'),
        )).thenAnswer((_) async => 42);

    // Mock bechaviour for provider's get method
    when(() => mockProvider.getMeasurement(
            any<String>(), any<String>(), any<DateTime>()))
        .thenAnswer((_) async => Measurement.empty());

    // Mock bechaviour for provider's update method
    when(() =>
            mockProvider.updateMeasurement(any<String>(), any<Measurement>()))
        .thenAnswer((_) async => 42);
  });

  test(
      'given no measurements exist, when creating new measurements, then only create a new row in measurements table',
      () async {
    // ? Arrange
    final mockMeasurement = Measurement(
        date: DateTime(2023, 9, 10),
        stationName: 'Station A',
        powerMeter: 123,
        p1Runtime: 12,
        p2Runtime: 12);
    when(() => mockProvider.getCurrentAndAdjacentMeasurementsForDate(
            table: any<String>(named: 'table'),
            stationName: any<String>(named: 'stationName'),
            date: any<DateTime>(named: 'date')))
        .thenAnswer((_) async => <String, Measurement>{});

    // ? Act
    final id = await repository.createOrUpdateMeasurement(mockMeasurement);

    // ? Assert
    verify(() => mockProvider.createMeasurement(
          table: MeasurementProvider.mainTable,
          measurement: mockMeasurement,
        )).called(1);
    verifyNever(() => mockProvider.createMeasurement(
          table: MeasurementProvider.tblDaily,
          measurement: mockMeasurement,
        ));
    expect(id, isNot(-1));
    expect(id, isA<int>());
  });
  test(
      'given  only previous measurement exist, when creating a measurement, then create a measurement in tblDaily as well',
      () async {
    // ? Arrange
    final previousMeasurement = Measurement(
      date: DateTime(2023, 9, 8),
      stationName: 'Station A',
      powerMeter: 110,
      p1Runtime: 10,
      p2Runtime: 10,
      time: const TimeOfDay(hour: 10, minute: 23),
    );
    final mockMeasurement = Measurement(
      time: const TimeOfDay(hour: 10, minute: 23),
      date: DateTime(2023, 9, 10),
      stationName: 'Station A',
      powerMeter: 123,
      p1Runtime: 12,
      p2Runtime: 12,
    );
    when(() => mockProvider.getCurrentAndAdjacentMeasurementsForDate(
            table: any<String>(named: 'table'),
            stationName: any<String>(named: 'stationName'),
            date: any<DateTime>(named: 'date')))
        .thenAnswer((_) async => <String, Measurement?>{
              'previous': previousMeasurement,
            });

    // ? Act
    final id = await repository.createOrUpdateMeasurement(mockMeasurement);

    // ? Assert
    verify(() => mockProvider.createMeasurement(
          table: MeasurementProvider.mainTable,
          measurement: mockMeasurement,
        )).called(1);
    verify(() => mockProvider.createMeasurement(
          table: MeasurementProvider.tblDaily,
          measurement:
              mockMeasurement.createNormalizedMeasurement(previousMeasurement),
        )).called(1);

    expect(id, isA<int>());
    expect(id, isNot(-1));
  });

  test(
      'given a previous and next measurement exist, when creating a measurement, then create a measurement in tblDaily and update the next normalised measurement in tblDaily as well',
      () async {
    // ? Arrange
    final previousMeasurement = Measurement(
      date: DateTime(2023, 9, 8),
      stationName: 'Station A',
      powerMeter: 110,
      p1Runtime: 10,
      p2Runtime: 10,
      time: const TimeOfDay(hour: 10, minute: 23),
    );
    final mockMeasurement = Measurement(
      time: const TimeOfDay(hour: 10, minute: 23),
      date: DateTime(2023, 9, 10),
      stationName: 'Station A',
      powerMeter: 123,
      p1Runtime: 12,
      p2Runtime: 12,
    );
    final nextMeasurement = Measurement(
      time: const TimeOfDay(hour: 10, minute: 23),
      date: DateTime(2023, 9, 12),
      stationName: 'Station A',
      powerMeter: 123,
      p1Runtime: 12,
      p2Runtime: 12,
    );
    when(() => mockProvider.getCurrentAndAdjacentMeasurementsForDate(
          table: any<String>(named: 'table'),
          stationName: any<String>(named: 'stationName'),
          date: any<DateTime>(named: 'date'),
        )).thenAnswer((_) async => <String, Measurement?>{
          'previous': previousMeasurement,
          'next': nextMeasurement,
        });

    // ? Act
    final id = await repository.createOrUpdateMeasurement(mockMeasurement);

    // ? Assert
    verify(() => mockProvider.createMeasurement(
          table: MeasurementProvider.mainTable,
          measurement: mockMeasurement,
        )).called(1);
    verify(() => mockProvider.createMeasurement(
          table: MeasurementProvider.tblDaily,
          measurement:
              mockMeasurement.createNormalizedMeasurement(previousMeasurement),
        )).called(1);
    verify(() => mockProvider.updateMeasurement(MeasurementProvider.tblDaily,
            nextMeasurement.createNormalizedMeasurement(mockMeasurement)))
        .called(1);

    expect(id, isA<int>());
    expect(id, isNot(-1));
  });
  test(
      'given  only next measurement exist, when creating a measurement, then create a measurement in tblDaily as well(new initial @2nd position)',
      () async {
    // ? Arrange
    final mockMeasurement = Measurement(
      time: const TimeOfDay(hour: 10, minute: 23),
      date: DateTime(2023, 9, 10),
      stationName: 'Station A',
      powerMeter: 123,
      p1Runtime: 12,
      p2Runtime: 12,
    );
    final nextMeasurement = Measurement(
      time: const TimeOfDay(hour: 10, minute: 23),
      date: DateTime(2023, 9, 12),
      stationName: 'Station A',
      powerMeter: 123,
      p1Runtime: 12,
      p2Runtime: 12,
    );

    when(() => mockProvider.getCurrentAndAdjacentMeasurementsForDate(
          table: any<String>(named: 'table'),
          stationName: any<String>(named: 'stationName'),
          date: any<DateTime>(named: 'date'),
        )).thenAnswer((_) async => <String, Measurement?>{
          'next': nextMeasurement,
        });

    // ? Act
    final id = await repository.createOrUpdateMeasurement(mockMeasurement);

    // ? Assert
    verify(() => mockProvider.createMeasurement(
          table: MeasurementProvider.mainTable,
          measurement: mockMeasurement,
        )).called(1);
    verify(() => mockProvider.createMeasurement(
          table: MeasurementProvider.tblDaily,
          measurement:
              nextMeasurement.createNormalizedMeasurement(mockMeasurement),
        )).called(1);

    expect(id, isA<int>());
    expect(id, isNot(-1));
  });
  test(
      'given  only current measurement exist, when creating a measurement, then update that measurement main table',
      () async {
    // ? Arrange
    final mockMeasurement = Measurement(
      time: const TimeOfDay(hour: 10, minute: 23),
      date: DateTime(2023, 9, 10),
      stationName: 'Station A',
      powerMeter: 123,
      p1Runtime: 12,
      p2Runtime: 12,
    );
    final mockNewMeasurement = Measurement(
      time: const TimeOfDay(hour: 10, minute: 23),
      date: DateTime(2023, 9, 10),
      stationName: 'Station A',
      powerMeter: 124,
      p1Runtime: 12,
      p2Runtime: 12,
    );

    when(() => mockProvider.getCurrentAndAdjacentMeasurementsForDate(
          table: any<String>(named: 'table'),
          stationName: any<String>(named: 'stationName'),
          date: any<DateTime>(named: 'date'),
        )).thenAnswer((_) async => <String, Measurement?>{
          'current': mockMeasurement,
        });

    // ? Act
    final id = await repository.createOrUpdateMeasurement(mockMeasurement);

    // ? Assert
    verifyNever(() => mockProvider.createMeasurement(
          table: MeasurementProvider.mainTable,
          measurement: mockNewMeasurement,
        ));
    verify(() => mockProvider.updateMeasurement(MeasurementProvider.mainTable,
        mockMeasurement.copyWith(id: mockNewMeasurement.id))).called(1);

    expect(id, isA<int>());
    expect(id, isNot(-1));
  });
  test(
      'given  current and previous measurements exist, when creating a measurement, then update that measurement main table and the corresponding normalised measurement in the tblDaily',
      () async {
    // ? Arrange
    final previousMeasurement = Measurement(
      id: 1,
      date: DateTime(2023, 9, 8),
      time: const TimeOfDay(hour: 10, minute: 23),
      stationName: 'Station A',
      powerMeter: 110,
      p1Runtime: 10,
      p2Runtime: 10,
    );
    final mockMeasurement = Measurement(
      id: 2,
      time: const TimeOfDay(hour: 10, minute: 23),
      date: DateTime(2023, 9, 10),
      stationName: 'Station A',
      powerMeter: 123,
      p1Runtime: 12,
      p2Runtime: 12,
    );
    final mockNewMeasurement = Measurement(
      time: const TimeOfDay(hour: 10, minute: 23),
      date: DateTime(2023, 9, 10),
      stationName: 'Station A',
      powerMeter: 124,
      p1Runtime: 15,
      p2Runtime: 16,
    );
    final existingNormalisedMeasurement =
        mockMeasurement.createNormalizedMeasurement(previousMeasurement);

    if (kDebugMode) {
      print(existingNormalisedMeasurement);
      print(mockNewMeasurement.copyWith(id: mockMeasurement.id));
      print(mockMeasurement.date.difference(previousMeasurement.date).inDays);
      print(mockMeasurement.date.difference(previousMeasurement.date).inDays);
      print(mockNewMeasurement
          .createNormalizedMeasurement(previousMeasurement)
          .copyWith(id: existingNormalisedMeasurement.id));
    }

    when(() => mockProvider.getCurrentAndAdjacentMeasurementsForDate(
          table: any<String>(named: 'table'),
          stationName: any<String>(named: 'stationName'),
          date: any<DateTime>(named: 'date'),
        )).thenAnswer((_) async => <String, Measurement?>{
          'previous': previousMeasurement,
          'current': mockMeasurement,
        });
    when(() => mockProvider.getMeasurement(MeasurementProvider.tblDaily,
            mockNewMeasurement.stationName, mockNewMeasurement.date))
        .thenAnswer((_) async => existingNormalisedMeasurement);

    // ? Act
    final id = await repository.createOrUpdateMeasurement(mockNewMeasurement);

    // ? Assert
    verifyNever(() => mockProvider.createMeasurement(
          table: MeasurementProvider.mainTable,
          measurement: mockNewMeasurement,
        ));
    verify(() => mockProvider.updateMeasurement(MeasurementProvider.mainTable,
        mockNewMeasurement.copyWith(id: mockMeasurement.id))).called(1);
    verify(() => mockProvider.updateMeasurement(
        MeasurementProvider.tblDaily,
        mockNewMeasurement
            .createNormalizedMeasurement(previousMeasurement)
            .copyWith(id: existingNormalisedMeasurement.id))).called(1);

    expect(id, isA<int>());
    expect(id, isNot(-1));
  });
  test(
      'given  current, previous and next measurements exist, when creating a measurement, then update that measurement main table and the corresponding current and next normalised measurement in the tblDaily',
      () async {
    // ? Arrange
    final previousMeasurement = Measurement(
      id: 1,
      date: DateTime(2023, 9, 8),
      time: const TimeOfDay(hour: 10, minute: 23),
      stationName: 'Station A',
      powerMeter: 110,
      p1Runtime: 10,
      p2Runtime: 10,
    );
    final mockMeasurement = Measurement(
      id: 2,
      time: const TimeOfDay(hour: 10, minute: 23),
      date: DateTime(2023, 9, 10),
      stationName: 'Station A',
      powerMeter: 123,
      p1Runtime: 12,
      p2Runtime: 12,
    );
    final nextMeasurement = Measurement(
      id: 3,
      time: const TimeOfDay(hour: 10, minute: 23),
      date: DateTime(2023, 9, 12),
      stationName: 'Station A',
      powerMeter: 123,
      p1Runtime: 12,
      p2Runtime: 12,
    );
    final mockNewMeasurement = Measurement(
      time: const TimeOfDay(hour: 10, minute: 23),
      date: DateTime(2023, 9, 10),
      stationName: 'Station A',
      powerMeter: 124,
      p1Runtime: 15,
      p2Runtime: 16,
    );
    final existingCurrentNormalisedMeasurement =
        mockMeasurement.createNormalizedMeasurement(previousMeasurement);
    final existingNextNormalisedMeasurement =
        nextMeasurement.createNormalizedMeasurement(mockMeasurement);

    when(() => mockProvider.getCurrentAndAdjacentMeasurementsForDate(
          table: any<String>(named: 'table'),
          stationName: any<String>(named: 'stationName'),
          date: any<DateTime>(named: 'date'),
        )).thenAnswer((_) async => <String, Measurement?>{
          'previous': previousMeasurement,
          'current': mockMeasurement,
          'next': nextMeasurement,
        });
    when(() => mockProvider.getMeasurement(MeasurementProvider.tblDaily,
            mockNewMeasurement.stationName, mockNewMeasurement.date))
        .thenAnswer((_) async => existingCurrentNormalisedMeasurement);

    when(() => mockProvider.getMeasurement(MeasurementProvider.tblDaily,
            mockNewMeasurement.stationName, nextMeasurement.date))
        .thenAnswer((_) async => existingNextNormalisedMeasurement);

    // ? Act
    final id = await repository.createOrUpdateMeasurement(mockNewMeasurement);

    // ? Assert
    verifyNever(() => mockProvider.createMeasurement(
          table: MeasurementProvider.mainTable,
          measurement: mockNewMeasurement,
        ));
    verify(() => mockProvider.updateMeasurement(MeasurementProvider.mainTable,
        mockNewMeasurement.copyWith(id: mockMeasurement.id))).called(1);
    verify(() => mockProvider.updateMeasurement(
        MeasurementProvider.tblDaily,
        mockNewMeasurement
            .createNormalizedMeasurement(previousMeasurement)
            .copyWith(id: existingCurrentNormalisedMeasurement.id))).called(1);
    verify(() => mockProvider.updateMeasurement(
        MeasurementProvider.tblDaily,
        nextMeasurement
            .createNormalizedMeasurement(mockNewMeasurement)
            .copyWith(id: existingNextNormalisedMeasurement.id))).called(1);

    expect(id, isA<int>());
    expect(id, isNot(-1));
  });
  test(
      'given  current and next measurements exist, when creating a measurement, then update that measurement main table and the corresponding current and next normalised measurement in the tblDaily',
      () async {
    // ? Arrange

    final mockMeasurement = Measurement(
      id: 2,
      time: const TimeOfDay(hour: 10, minute: 23),
      date: DateTime(2023, 9, 10),
      stationName: 'Station A',
      powerMeter: 123,
      p1Runtime: 12,
      p2Runtime: 12,
    );
    final nextMeasurement = Measurement(
      id: 3,
      time: const TimeOfDay(hour: 10, minute: 23),
      date: DateTime(2023, 9, 12),
      stationName: 'Station A',
      powerMeter: 123,
      p1Runtime: 12,
      p2Runtime: 12,
    );
    final mockNewMeasurement = Measurement(
      time: const TimeOfDay(hour: 10, minute: 23),
      date: DateTime(2023, 9, 10),
      stationName: 'Station A',
      powerMeter: 124,
      p1Runtime: 15,
      p2Runtime: 16,
    );
    final existingNextNormalisedMeasurement =
        nextMeasurement.createNormalizedMeasurement(mockMeasurement);

    when(() => mockProvider.getCurrentAndAdjacentMeasurementsForDate(
          table: any<String>(named: 'table'),
          stationName: any<String>(named: 'stationName'),
          date: any<DateTime>(named: 'date'),
        )).thenAnswer((_) async => <String, Measurement?>{
          'current': mockMeasurement,
          'next': nextMeasurement,
        });

    when(() => mockProvider.getMeasurement(MeasurementProvider.tblDaily,
            mockNewMeasurement.stationName, nextMeasurement.date))
        .thenAnswer((_) async => existingNextNormalisedMeasurement);

    // ? Act
    final id = await repository.createOrUpdateMeasurement(mockNewMeasurement);

    // ? Assert
    verifyNever(() => mockProvider.createMeasurement(
          table: MeasurementProvider.mainTable,
          measurement: mockNewMeasurement,
        ));
    verify(() => mockProvider.updateMeasurement(MeasurementProvider.mainTable,
        mockNewMeasurement.copyWith(id: mockMeasurement.id))).called(1);

    verify(() => mockProvider.updateMeasurement(
        MeasurementProvider.tblDaily,
        nextMeasurement
            .createNormalizedMeasurement(mockNewMeasurement)
            .copyWith(id: existingNextNormalisedMeasurement.id))).called(1);

    expect(id, isA<int>());
    expect(id, isNot(-1));
  });
}
