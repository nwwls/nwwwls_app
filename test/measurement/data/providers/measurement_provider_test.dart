import 'package:flutter_test/flutter_test.dart';
import 'package:nwwwls_app/measurement/data/models/measurement.dart';
import 'package:nwwwls_app/measurement/data/providers/measurement_provider.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  group('MeasurementProvider Tests', () {
    late MeasurementProvider provider;
    late Database db;

    setUpAll(() async {
      sqfliteFfiInit();
      databaseFactory = databaseFactoryFfi;
      provider = MeasurementProvider();
      db = await provider.initializeDatabase();
    });
    setUp(() async {
      await db.execute('DELETE FROM ${MeasurementProvider.mainTable}');
      await db.execute('DELETE FROM ${MeasurementProvider.tblDaily}');
    });
    tearDownAll(() async {
      db.close();
    });

    test('Database Initialization', () async {
      final db = await provider.initializeDatabase();
      expect(db, isNotNull);
      expect(await db.getVersion(), 1); // Update with your expected version
    });

    test('Table creation', () async {
      final tables = await db
          .query('sqlite_master', where: 'type = ?', whereArgs: ['table']);
      final tableNames =
          tables.map((table) => table['name'] as String).toList();
      tableNames.remove('sqlite_sequence');

      expect(tableNames.contains(MeasurementProvider.mainTable), isTrue);
      expect(tableNames.contains(MeasurementProvider.tblDaily), isTrue);
    });

    test('Create New Unique Record', () async {
      final measurement = Measurement(
        date: DateTime.now(),
        stationName: 'Test Station',
        powerMeter: 100,
        isSynchronized: false,
      );

      final createdId = await provider.createMeasurement(
          table: MeasurementProvider.mainTable, measurement: measurement);
      expect(createdId, isNot(-1));

      // Retrieve the record from the database and compare
      final retrievedMeasurement = await provider.getMeasurement(
        MeasurementProvider.mainTable,
        'Test Station',
        measurement.date,
      );
      expect(retrievedMeasurement, isNotNull);
      expect(retrievedMeasurement!.id, equals(createdId));
    });

    test('Retrieve Measurement', () async {
      final measurement = Measurement(
        date: DateTime.now(),
        stationName: 'Test Station',
        powerMeter: 100,
        isSynchronized: false,
      );

      final createdId = await provider.createMeasurement(
          table: MeasurementProvider.mainTable, measurement: measurement);

      final retrievedMeasurement = await provider.getMeasurement(
        MeasurementProvider.mainTable,
        'Test Station',
        measurement.date,
      );

      expect(retrievedMeasurement, isNotNull);
      expect(retrievedMeasurement!.id, equals(createdId));
    });

    test('Update Existing Record', () async {
      final measurement = Measurement(
        date: DateTime.now(),
        stationName: 'Test Station',
        powerMeter: 100,
        isSynchronized: false,
      );

      final createdId = await provider.createMeasurement(
          table: MeasurementProvider.mainTable, measurement: measurement);

      final updatedMeasurement = Measurement(
        id: createdId,
        date: measurement.date,
        stationName: 'Updated Station',
        powerMeter: 150,
        isSynchronized: true,
      );

      final updateResult = await provider.updateMeasurement(
          MeasurementProvider.mainTable, updatedMeasurement);
      expect(updateResult, equals(1)); // Expecting 1 row updated

      // Retrieve the updated record from the database and compare
      final retrievedUpdatedMeasurement = await provider.getMeasurement(
        MeasurementProvider.mainTable,
        'Updated Station',
        measurement.date,
      );
      expect(retrievedUpdatedMeasurement, isNotNull);
      expect(retrievedUpdatedMeasurement!.id, equals(createdId));
    });

    test(
        'getCurrentAndAdjacentMeasurement returns 3, 2, 1 and 0 measurements...',
        () async {
      // Arrange: Create some measurement data
      final measurementsToCreate = [
        Measurement(
          date: DateTime(2023, 1, 1),
          stationName: 'Station A',
          powerMeter: 100,
          isSynchronized: false,
        ),
        Measurement(
          date: DateTime(2023, 1, 2),
          stationName: 'Station A',
          powerMeter: 150,
          isSynchronized: true,
        ),
        Measurement(
          date: DateTime(2023, 1, 4), // Gap in dates
          stationName: 'Station A',
          powerMeter: 180,
          isSynchronized: false,
        ),
        Measurement(
          date: DateTime(2023, 1, 5),
          stationName: 'Station A',
          powerMeter: 200,
          isSynchronized: false,
        ),
        Measurement(
          date: DateTime(2023, 1, 3),
          stationName: 'Station B',
          powerMeter: 200,
          isSynchronized: false,
        ),
      ];

      final provider = MeasurementProvider();

      // Act: Create the measurements in the provider
      for (final measurement in measurementsToCreate) {
        final createdId = await provider.createMeasurement(
            table: MeasurementProvider.mainTable, measurement: measurement);
        expect(createdId, isNot(-1));
      }

      // Arrange: Specify the stationName and date for retrieval
      const stationNameToRetrieve = 'Station A';
      final dateToRetrieve = DateTime(2023, 1, 4);

      // Act: Retrieve current and adjacent measurements
      final retrievedMeasurements1 =
          await provider.getCurrentAndAdjacentMeasurementsForDate(
        table: MeasurementProvider.mainTable,
        stationName: stationNameToRetrieve,
        date: dateToRetrieve,
      );
      final retrievedMeasurements2 =
          await provider.getCurrentAndAdjacentMeasurementsForDate(
        table: MeasurementProvider.mainTable,
        stationName: stationNameToRetrieve,
        date: dateToRetrieve.add(const Duration(days: 1)),
      );
      final retrievedMeasurements3 =
          await provider.getCurrentAndAdjacentMeasurementsForDate(
        table: MeasurementProvider.mainTable,
        stationName: 'Station B',
        date: DateTime(2023, 1, 3),
      );

      final retrievedMeasurements4 =
          await provider.getCurrentAndAdjacentMeasurementsForDate(
        table: MeasurementProvider.mainTable,
        stationName: 'Station A',
        date: DateTime(2023, 1, 1),
      );

      final retrievedMeasurements5 =
          await provider.getCurrentAndAdjacentMeasurementsForDate(
        table: MeasurementProvider.mainTable,
        stationName: 'Station C',
        date: DateTime(2023, 1, 3),
      );

      // Assert: Check if the retrieved measurements match the expected outcome
      expect(retrievedMeasurements1, contains('previous'));
      expect(retrievedMeasurements1, contains('current'));
      expect(retrievedMeasurements1, contains('next'));

      expect(retrievedMeasurements2, contains('previous'));
      expect(retrievedMeasurements2, contains('current'));

      expect(retrievedMeasurements3, contains('current'));

      expect(retrievedMeasurements4, contains('current'));
      expect(retrievedMeasurements4, contains('next'));

      expect(retrievedMeasurements5, isEmpty);
    });

    test('getMeasurementsForStationInDateRange returns created measurements',
        () async {
      // Arrange: Create some measurement data
      final measurementsToCreate = [
        Measurement(
          date: DateTime(2023, 1, 1),
          stationName: 'Station A',
          powerMeter: 100,
          isSynchronized: false,
        ),
        Measurement(
          date: DateTime(2023, 1, 2),
          stationName: 'Station A',
          powerMeter: 150,
          isSynchronized: true,
        ),
        Measurement(
          date: DateTime(2023, 1, 3),
          stationName: 'Station B',
          powerMeter: 200,
          isSynchronized: false,
        ),
      ];

      final provider = MeasurementProvider();

      // Act: Create the measurements in the provider
      for (final measurement in measurementsToCreate) {
        final createdId = await provider.createMeasurement(
            table: MeasurementProvider.mainTable, measurement: measurement);
        expect(createdId, isNot(-1));
      }

      // Arrange: Specify the stationName and date range for retrieval
      const stationNameToRetrieve = 'Station A';
      final startDate = DateTime(2023, 1, 1);
      final endDate = DateTime(2023, 1, 2);

      // Act: Retrieve measurements for the specified station and date range
      final retrievedMeasurements =
          await provider.getMeasurementsForStationInDateRange(
        MeasurementProvider.mainTable,
        stationNameToRetrieve,
        startDate,
        endDate,
      );

      // Assert: Check if the retrieved measurements match the created measurements
      expect(retrievedMeasurements,
          hasLength(2)); // Expecting 2 measurements for Station A
      expect(
          retrievedMeasurements?.every((measurement) =>
              measurement.stationName == stationNameToRetrieve),
          isTrue);

      expect(
          retrievedMeasurements?.every((measurement) =>
              measurement.date.isAtSameMomentAs(startDate) ||
              measurement.date.isAfter(startDate) &&
                  measurement.date
                      .isBefore(endDate.add(const Duration(days: 1)))),
          isTrue);
    });
  });
}
